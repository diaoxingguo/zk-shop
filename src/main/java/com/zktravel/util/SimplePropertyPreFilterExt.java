
package com.zktravel.util;

import java.util.HashSet;
import java.util.Set;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.PropertyPreFilter;

/**
 *
 */
public class SimplePropertyPreFilterExt implements PropertyPreFilter{
	

    private final Class<?>    clazz;
    private final Set<String> includes = new HashSet<String>();
    private final Set<String> excludes = new HashSet<String>();

    public SimplePropertyPreFilterExt(String[] includesFields, String[] exludesFields){
        this(null, includesFields, exludesFields);
    }

    public SimplePropertyPreFilterExt(Class<?> clazz, String[] includesFields, String[] exludesFields){
        super();
        this.clazz = clazz;
        if(includesFields!=null){
	        for (String item : includesFields) {
	            if (item != null) {
	                this.includes.add(item);
	            }
	        }
        }
        
        if(exludesFields!=null){
	        for (String item : exludesFields) {
	            if (item != null) {
	                this.excludes.add(item);
	            }
	        }
        }
    }
    
    public Class<?> getClazz() {
        return clazz;
    }

    public Set<String> getIncludes() {
        return includes;
    }

    public Set<String> getExcludes() {
        return excludes;
    }

    public boolean apply(JSONSerializer serializer, Object source, String name) {
        if (source == null) {
            return true;
        }

        if (clazz != null && !clazz.isInstance(source)) {
            return true;
        }

        if (this.excludes.contains(name)) {
            return false;
        }

        if (includes.size() == 0 || includes.contains(name)) {
            return true;
        }

        return false;
    }
}
