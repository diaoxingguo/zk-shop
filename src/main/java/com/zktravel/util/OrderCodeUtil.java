package com.zktravel.util;

import java.util.Random;

import com.odianyun.util.date.DatetimeUtils;

public class OrderCodeUtil {

    public static String getOrderCode(Long userId, String prefix) {
        String rq = DatetimeUtils.getCurDate("yyMMdd"); // 6位日期 YYMMDD
        int randomNum = random8(); // 8位随机数
        String userId3 = splitUserIdtoThreelength(userId);
        String check = getCheckCode(rq, randomNum + "", userId3);
        String code = rq + randomNum + check + userId3;
        return prefix + code;
    }

    public static String getOrderCodeSimple(Long userId) {
        String prefix = "SH";
        String rq = DatetimeUtils.getCurDate("yyMMddHHmm"); // 10位日期 YYMMDD
        int randomNum = random4(); // 4位随机数
        String userId3 = splitUserIdtoThreelength(userId);
        String check = getCheckCode(rq, randomNum + "", userId3);
        String code = rq + randomNum + check + userId3;
        return prefix + code;
    }

    /**
     * @param key 订单主键
     * @return 生成order_id 用于支付的out_trade_no
     */
    public static String getOrderId(Long key) {
        /*
         * 订单id 用于支付 out_trade_no
         * 生成规则
         * SH2018041812161041201151
         * 前缀 SH (回调判断订单类型)
         * 时间 20180418121610
         * 随机数 4120
         * 订单主键 1151 (用于支付回调定位订单用)
         */
        String prefix = "SH";
        String rq = DatetimeUtils.getCurDate("yyyyMMddHHmmss"); // 12位日期
        int randomNum = random4(); // 4位随机数
        return prefix + rq + String.format("%04d", randomNum) + String.format("%d", key);
    }

    /**
     * @return
     * @description <pre>
     * 获取订单编号
     * </pre>
     */
    public static String getOrderCode(Long userId) {
        String rq = DatetimeUtils.getCurDate("yyMMdd"); // 6位日期 YYMMDD
        int randomNum = random8(); // 8位随机数
        String userId3 = splitUserIdtoThreelength(userId);
        String check = getCheckCode(rq, randomNum + "", userId3);
        return rq + randomNum + check + userId3;
    }

    /**
     * @param rq
     * @param randomNum
     * @param userId3
     * @return
     * @description <pre>
     * 获取校验位
     * </pre>
     */
    private static String getCheckCode(String rq, String randomNum, String userId3) {
        int rqInt = Integer.parseInt(rq);
        int userIdInt = Integer.parseInt(userId3);
        int randomNumInt = Integer.parseInt(randomNum);
        int complementation = (userIdInt + randomNumInt) / rqInt;
        return getLast(complementation);
    }

    private static String getLast(int complementation) {
        String str = complementation + "";
        return str.substring(str.length() - 1, str.length());
    }

    public static boolean checkOrderCode(String code) {

        if (code.length() != 18) {
            return false;
        }

        String rq = code.substring(0, 6); // 6位日期 YYMMDD
        String randomNum = code.substring(6, 14); // 8位随机数
        String checkCode = code.substring(14, 15);
        String userId3 = code.substring(15);

        String checkCodeNum = getCheckCode(rq, randomNum, userId3);
        if (checkCode.equals(checkCodeNum)) {
            return true;
        }
        return false;
    }

    /**
     * @return
     * @description <pre>
     * 随机生成8位整数
     * </pre>
     */
    public static int random8() {
        Random random = new Random();
        return 10000000 + random.nextInt(90000000);
    }

    /**
     * @return
     * @description <pre>
     * 随机生成4位整数
     * </pre>
     */
    public static int random4() {
        Random random = new Random();
        return 1000 + random.nextInt(9000);
    }

    /**
     * @param userId
     * @return
     * @description <pre>
     * 截取三位长度的用户id
     * </pre>
     */
    public static String splitUserIdtoThreelength(Long userId) {

        if ((userId + "").length() == 3) {
            return userId + "";
        } else if ((userId + "").length() > 3) {
            return (userId + "").substring((userId + "").length() - 3, (userId + "").length());
        } else {
            return String.format("%03d", userId);
        }
    }

    public static void main(String[] args) {
        String rq = getOrderCodeSimple(3224173L);
        System.out.println(rq);
    }

}
