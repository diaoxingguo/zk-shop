package com.zktravel.util;

import java.util.function.BiConsumer;

/**
 * 四参数消费者，比{@link BiConsumer}多两个参数。
 * 
 * @param <A>
 * @param <B>
 * @param <C>
 * @param <D>
 * @author Zhang Xiaoye
 * @date 2017年12月22日 下午6:06:50
 */
@FunctionalInterface
public interface QuadConsumer<A, B, C, D> {
	
	public void accept(A a, B b, C c, D d);

}
