package com.zktravel.util;

public class StringConcatUtils {

    /**
     * @param resourceStr 源字符串
     * @param conStr      需要拼接的字符串
     * @param length      拼接长度
     * @return String
     * @throws
     * @Title: concatString
     * @Description: 拼接字符串
     * @author diaoxingguo
     * @date 2018年6月23日
     */
    public static String concatString(String resourceStr, String conStr, Integer length) {
        if (null == length) {
            length = 30;
        }

        if (resourceStr.length() >= length) {
            return resourceStr;
        }

        StringBuilder builder = new StringBuilder();
        builder.append(resourceStr);
        // 处理第一个，
        if (!resourceStr.equals("")) {
            builder.append(",");
        }
        builder.append(conStr);

        String result = builder.toString();
        if (result.length() > length) {
            result = result.substring(0, length - 1) + "...";
        }
        return result;
    }

}