package com.zktravel.util;

import java.util.function.BiConsumer;

/**
 * 三参数消费者，比{@link BiConsumer}多一个参数。
 * 
 * @param <A>
 * @param <B>
 * @param <C>
 * @author Zhang Xiaoye
 * @date 2017年12月22日 下午6:05:39
 */
@FunctionalInterface
public interface TriConsumer<A, B, C> {
	
	public void accept(A a, B b, C c);

}
