package com.zktravel.util;

import java.util.regex.Pattern;

/**
 * 日期验证，含闰年
 * 
 * 
 * @author Zhang Xiaoye
 * @date 2017年9月12日 下午3:39:54
 */
public class DateValidation {
	
	public static final String P_YEAR = "(19\\d{2}|20\\d{2}|21\\d{2})";
	public static final String P_MONTH = "(10|11|12|0?[1-9])";
	public static final String P_DAY = "([12]\\d|30|31|0?[1-9])";
	
	public static final String P_HOUR = "(1\\d|2[0-3]|0?\\d)";
	public static final String P_MINTUE = "([1-5]\\d|0?\\d)";
	public static final String P_SECOND = "([1-5]\\d|0?\\d)";
	
	public static final Pattern PATTERN = Pattern.compile("((^((1[8-9]\\d{2})|([2-9]\\d{3}))([-\\/\\._])(10|12|0?[13578])([-\\/\\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\\d{2})|([2-9]\\d{3}))([-\\/\\._])(11|0?[469])([-\\/\\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\\d{2})|([2-9]\\d{3}))([-\\/\\._])(0?2)([-\\/\\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([3579][26]00)([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([1][89][0][48])([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([2-9][0-9][0][48])([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([1][89][2468][048])([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([2-9][0-9][2468][048])([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([1][89][13579][26])([-\\/\\._])(0?2)([-\\/\\._])(29)$)|(^([2-9][0-9][13579][26])([-\\/\\._])(0?2)([-\\/\\._])(29)$))");

	/**
	 * 日期字符串是否合法，含闰年的验证。支持的年月日分隔符：[-/._]
	 * 
	 * @param dateDashStr 日期字符串
	 * @return true: 合法 false: 不合法
	 * @author Zhang Xiaoye
	 * @date 2017年9月12日 下午3:41:18
	 */
	public static boolean isValid(String dateDashStr){
		return PATTERN.matcher(dateDashStr).matches();
	}
	
	/**
	 * 只支持 yyyy MM dd HH mm ss 来组合
	 * 
	 * @param format
	 * @return
	 * @author Zhang Xiaoye
	 * @date 2017年9月28日 下午6:56:11
	 */
	public static final Pattern ofFormat(String format){
		String pattern = format.replace("-", "\\-").replace("/", "\\/")
				.replace("yyyy", P_YEAR).replace("MM", P_MONTH).replace("dd", P_DAY)
				.replace("HH", P_HOUR).replace("mm", P_MINTUE).replace("ss", P_SECOND);
		;
		return Pattern.compile(pattern);
	}
	
}
