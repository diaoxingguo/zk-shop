package com.zktravel.util;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;

import com.odianyun.util.spring.SpringApplicationContext;

public abstract class LangUtils {
    private static Logger logger = LoggerFactory.getLogger(LangUtils.class);
    
    /**
     * 多个code的值连在一起返回
     * @param codes
     * @return
     */
    public static String joinMsg(String... codes) {
    	Collection<MessageSource> ms = getMessageSources();
        Locale locale = LocaleContextHolder.getLocale();
        
        StringBuilder msg = new StringBuilder();
        for (String code : codes) {
            msg.append(getMsg(ms, code, null, locale, code));
        }
        
        return msg.toString();
    }

    public static String getMsg(String code, String defaultMsg) {
        return getMsg(getMessageSources(), code, null, LocaleContextHolder.getLocale(), defaultMsg);
    }
    
    public static String getMsg(String code, String defaultMsg, Object[] args) {
        return getMsg(getMessageSources(), code, args, LocaleContextHolder.getLocale(), defaultMsg);
    }

    public static String getMsgWithDefault(String code) {
        return getMsg(getMessageSources(), code, null, LocaleContextHolder.getLocale(), code);
    }
    
    public static String getMsgWithDefault(String code, Object[] args) {
        return getMsg(getMessageSources(), code, args, LocaleContextHolder.getLocale(), code);
    }
    
    public static String getMsgWithDefault(String code, Locale locale, Object[] args) {
        return getMsg(getMessageSources(), code, args, locale, code);
    }
    
    private static String getMsg(Collection<MessageSource> mss, String code, Object[] args, Locale locale, 
    		String msgOnNotExists) {
        String msg = null;
        
    	for (MessageSource ms : mss) {
    		msg = getMsg(ms, code, args, locale);
    		if (msg == null) {
            	msg = getMsg(ms, code, args, getLocaleOnlyLanguage(locale));
    		}
    		if (msg != null) break;
    	}
    	
        if (msg == null) {
            logger.debug("code: "+code+" is not found in Message");
            return msgOnNotExists;
        }
        return msg;
    }
    
    private static String getMsg(MessageSource ms, String code, Object[] args, Locale locale) {
    	try {
            return ms.getMessage(code,  args, getLocaleOnlyLanguage(locale));
        } catch (NoSuchMessageException e) {
            return null;
        }
    }
    
    private static Locale getLocaleOnlyLanguage(Locale locale) {
        return new Locale(locale.getLanguage());
    }
    
    private static Collection<MessageSource> getMessageSources() {
        Map<String, MessageSource> map = SpringApplicationContext.getCtx().getBeansOfType(MessageSource.class);
        return map.values();
    }
}
