package com.zktravel.core.bean;

import java.beans.Transient;
import java.util.List;
import java.util.Map;

import org.springframework.util.Assert;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.odianyun.util.db.query.Sort;
import com.odianyun.util.value.ValueUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("查询参数")
public class QueryArgs {
	@ApiModelProperty(value="排序",hidden=true)
	private List<Sort> sorts;
	@ApiModelProperty(value="过滤参数列表",  dataType="java.util.Map")
	private Map<String, Object> filters = Maps.newHashMap();
	public QueryParamConvertor convertor;
	private String[] joinFields;
	
	public QueryArgs() {
		convertor = new QueryParamConvertor(this);
	}

	public QueryArgs withJoinFields(String... joinFields) {
		this.joinFields = joinFields;
		return this;
	}
	public void setJoinFields(String[] joinFields) {
		this.joinFields = joinFields;
	}
	public boolean hasJoinField(String field) {
		if (joinFields == null) return false;
		for (String f : joinFields) {
			if (f.equals(field)) return true;
		}
		return false;
	}
	public boolean hasAnyJoinField() {
		return joinFields != null && joinFields.length > 0;
	}

	public List<Sort> getSorts() {
		return sorts;
	}
	public void setSorts(List<Sort> sorts) {
		this.sorts = sorts;
	}
	public QueryArgs withSort(Sort sort) {
		Assert.notNull(sort, "Parameter sort is required");
		if (sorts == null) {
			sorts = Lists.newArrayList();
		}
		sorts.add(sort);
		return this;
	}
	public Map<String, Object> getFilters() {
		return filters;
	}
	public void setFilters(Map<String, Object> filters) {
		if (filters != null) this.filters = filters;
	}
	
	public boolean hasFilter(String key) {
		return filters.containsKey(key);
	}
	
	public QueryArgs with(String key, Object value) {
		filters.put(key, value);
		return this;
	}
	
	public Object get(String key) {
		return filters.get(key);
	}
	
	public <T> T get(String key, Class<T> typeClass) {
		Object value = filters.get(key);
		if (value == null) return null; 
		return convert(value, typeClass);
	}

    protected <T> T convert(Object obj, Class<T> typeClass) {
    	return ValueUtils.convert(obj, typeClass);
    }
    
    @Transient
    public QueryParamConvertor getConvertor() {
    	return convertor;
    }
    @Transient
    public QueryParamConvertor getConvertor(String...includeKeys) {
    	if (includeKeys != null && includeKeys.length > 0) {
    		return convertor.clone().withIncludeKeys(includeKeys);
    	}
    	return convertor;
    }
    @Transient
    public QueryParamConvertor getConvertorWithIgnoreKeys(String...ignoreKeys) {
    	if (ignoreKeys != null && ignoreKeys.length > 0) {
    		return convertor.clone().ignore(ignoreKeys);
    	}
    	return convertor;
    }
    
    /**
     * @Title: covertComma2List
     * @Description: 应为在导出的时候，如果是数组对象，会用","拼接，需要去","转成list
     * @param key 参数说明
     * @return 返回类型
     * @throws 
     * @author tangxiu
     * @date 2017年8月16日
     */
    @Transient
    public void covertComma2Array(String key) {
    	if (hasFilter(key)) {
    		String[] items = get(key, String.class).split("[,，]");
    		for (int i=0; i<items.length; i++) {
    			items[i] = items[i].trim();
    		}
    		with(key, items);
    	}
    }
    
    public QueryArgs with2Array(String key) {
    	covertComma2Array(key);
    	return this;
    }
    
    public QueryArgs withLikes(String... key) {
    	convertor.withLikeKeys(key);
    	return this;
    }
    
    public QueryArgs withLeftLikes(String... key) {
    	convertor.withLeftLikeKeys(key);
    	return this;
    }
    
    public QueryArgs withRightLike(String... key) {
    	convertor.withRightLikeKeys(key);
    	return this;
    }
    
    @Override
    public String toString() {
    	return JSON.toJSONString(this);
    }
}
