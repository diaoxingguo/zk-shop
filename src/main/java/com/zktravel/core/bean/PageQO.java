package com.zktravel.core.bean;

public class PageQO<T> extends Page implements IPage {
	private T t;

	public T getT() {
		return t;
	}
	public void setT(T t) {
		this.t = t;
	}

	public PageQO() {}
	
	public PageQO(int page, int limit, T t) {
		super(page, limit);
		this.t = t;
	}
	
}
