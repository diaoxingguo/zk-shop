package com.zktravel.core.bean;

public interface IPage {

	int getPage();
	
	int getLimit();
}
