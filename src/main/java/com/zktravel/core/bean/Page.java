package com.zktravel.core.bean;

import io.swagger.annotations.ApiModelProperty;

public class Page implements IPage {
	public final static int DEFAULT_LIMIT = 200;
	public final static int DEFAULT_PAGE = 1;
	
	@ApiModelProperty("页码")
	private int page = DEFAULT_PAGE;
	@ApiModelProperty("页大小")
	private int limit = DEFAULT_LIMIT;

	public Page() {}
	
	public Page(int page, int limit) {
		this.page = page;
		this.limit = limit;
	}
	
	public int getPage() {
		return page > 0 ? page : DEFAULT_PAGE;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit > 0 ? limit : DEFAULT_LIMIT;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
}
