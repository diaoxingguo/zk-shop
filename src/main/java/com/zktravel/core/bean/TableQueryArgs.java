package com.zktravel.core.bean;

public class TableQueryArgs extends PageQueryArgs {
	private String cols;

	public String getCols() {
		return cols;
	}

	public void setCols(String cols) {
		this.cols = cols;
	}
	
}
