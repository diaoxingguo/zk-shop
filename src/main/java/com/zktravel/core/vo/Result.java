package com.zktravel.core.vo;

import com.zktravel.enums.CodeEnum;
import com.zktravel.enums.ICodeMessage;

public class Result {
	public final static Result OK = new Result(CodeEnum.OK);
	public final static Result ERROR = new Result(CodeEnum.ERROR);
	
	public final static Result error(String message) {
		return new Result(CodeEnum.ERROR.getCode(), message);
	}
	
	public final static Result success(String message) {
		return new Result(CodeEnum.OK.getCode(), message);
	}
	
	private Integer code;
	private String msg;
	private Long timestamp;

	public Result() {}
	
	public Result(ICodeMessage codeMsg) {
		this(codeMsg.getCode(), codeMsg.getMessage());
	}
	
	public Result(ICodeMessage codeMsg, Object... params) {
		this(codeMsg.getCode(), codeMsg.getMessage());
	}
	
	public Result(Integer code, String message) {
		this.code = code;
		this.msg = message;
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Long getTimestamp() {
		timestamp = System.currentTimeMillis()/1000;
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isSuccess() {
		return CodeEnum.OK.getCode().equals(code);
	}
	
}
