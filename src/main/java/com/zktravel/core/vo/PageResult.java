package com.zktravel.core.vo;

import java.util.List;

import com.odianyun.util.db.query.PageVO;
import com.zktravel.enums.CodeEnum;
import com.zktravel.enums.ICodeMessage;

public class PageResult<T> extends ListResult<T> {
	private long total;
	private long totalPages;
	
	public static <T> PageResult<T> ok(List<T> data) {
		return new PageResult<T>(data);
	}
	
	public static <T> PageResult<T> error(List<T> data) {
		return new PageResult<T>(CodeEnum.ERROR, data);
	}

	public static <T> PageResult<T> ok(PageVO<T> data) {
		return new PageResult<T>(data.getList()).withTotal(data.getTotal()).withTotalPages(data.getTotalPages());
	}
	
	public static <T> PageResult<T> error(PageVO<T> data) {
		return new PageResult<T>(CodeEnum.ERROR, data.getList()).withTotal(data.getTotal()).withTotalPages(data.getTotalPages());
	}
	
	public PageResult(List<T> data) {
		super(CodeEnum.OK, data);
	}
	
	public PageResult(ICodeMessage codeMsg, List<T> data) {
		super(codeMsg, data);
	}
	
	public PageResult(Integer code, String message, List<T> data) {
		super(code, message, data);
	}
	
	public PageResult<T> withTotal(long total) {
		this.total = total;
		return this;
	}

	public PageResult<T> withTotalPages(long totalPages) {
		this.totalPages = totalPages;
		return this;
	}
	
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public long getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}
}
