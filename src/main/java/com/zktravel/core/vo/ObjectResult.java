package com.zktravel.core.vo;

import com.zktravel.enums.CodeEnum;
import com.zktravel.enums.ICodeMessage;

public class ObjectResult<T> extends Result {
	private T data;
	
	public static <T> ObjectResult<T> ok(T data) {
		return new ObjectResult<T>(data);
	}
	
	public static <T> ObjectResult<T> error(T data) {
		return new ObjectResult<T>(CodeEnum.ERROR, data);
	}
	
	public ObjectResult(T data) {
		this(CodeEnum.OK, data);
	}
	
	public ObjectResult(ICodeMessage codeMsg, T data) {
		super(codeMsg, data);
		this.data = data;
	}
	
	public ObjectResult(Integer code, String message, T data) {
		super(code, message);
		this.data = data;
	}
	
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
}
