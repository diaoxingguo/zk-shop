package com.zktravel.core.vo;

import java.util.List;

import com.zktravel.enums.CodeEnum;
import com.zktravel.enums.ICodeMessage;

public class ListResult<T> extends Result {
	private List<T> data;
	
	public static <T> ListResult<T> ok(List<T> data) {
		return new ListResult<T>(data);
	}
	
	public static <T> ListResult<T> error(List<T> data) {
		return new ListResult<T>(CodeEnum.ERROR, data);
	}
	
	public ListResult(List<T> data) {
		this(CodeEnum.OK, data);
	}
	
	public ListResult(ICodeMessage codeMsg, List<T> data) {
		super(codeMsg);
		this.data = data;
	}
	
	public ListResult(Integer code, String message, List<T> data) {
		super(code, message);
		this.data = data;
	}

	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
	
}