package com.zktravel.core.exception;

import com.zktravel.enums.CodeEnum;

public class VisibleException extends ServiceException implements ResultCodeMessage{
	private static final long serialVersionUID = 1L;
	
	private final Integer code;

	public VisibleException(Integer code, String message) {
		super(message);
		this.code = code;
	}
	
	public VisibleException(CodeEnum codeEnum) {
		super(codeEnum.getMessage());
		this.code = codeEnum.getCode();
	}
	
	public VisibleException(Integer code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}

	@Override
	public Integer getCode() {
		return code;
	}

	@Override
	public String getMsg() {
		return getMessage();
	}
}
