package com.zktravel.core.exception;

public interface ResultCodeMessage {
	Integer getCode();

    String getMsg();
    
    public static class SimpleMessage implements ResultCodeMessage{
    	private Integer code;
    	private String message;
		@Override
		public Integer getCode() {
			return code;
		}
		@Override
		public String getMsg() {
			return message;
		}
		public static ResultCodeMessage of(Integer code, String message) {
			SimpleMessage ret = new SimpleMessage();
			ret.code = code;
			ret.message = message;
			return ret;
		}
    }
}
