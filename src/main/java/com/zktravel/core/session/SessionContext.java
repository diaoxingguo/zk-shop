package com.zktravel.core.session;

import javax.servlet.http.HttpServletRequest;

public class SessionContext {
    private static ThreadLocal<Long> userId = new InheritableThreadLocal<>();
    private static ThreadLocal<Long> shopId = new InheritableThreadLocal<>();
    private static ThreadLocal<String> sessionKeyHolder = new InheritableThreadLocal<>();
    private static ThreadLocal<HttpServletRequest> requestHolder = new InheritableThreadLocal<>();

    public static void setShopId(Long sId) {
        SessionContext.shopId.set(sId);
    }

    public static Long getShopId() {
        return SessionContext.shopId.get();
    }

    public static void setUserId(Long uId) {
        SessionContext.userId.set(uId);
    }

    public static Long getUserId() {
        return SessionContext.userId.get();
    }

    public static void setRequest(HttpServletRequest request) {
        requestHolder.set(request);
    }

    public static HttpServletRequest getRequest() {
        return requestHolder.get();
    }

    public static void setSessionKey(String sessionKey) {
        sessionKeyHolder.set(sessionKey);
    }

    public static String getSessionKey() {
        return sessionKeyHolder.get();
    }

    public static void clear() {
        userId.remove();
        shopId.remove();
        sessionKeyHolder.remove();
        requestHolder.remove();
    }
}
