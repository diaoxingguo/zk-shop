package com.zktravel.service.category;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import com.odianyun.util.BeanUtils;
import com.zktravel.constants.CacheConstant;
import com.zktravel.mapper.PlCategoryMapper;
import com.zktravel.mapper.TaProductMapper;
import com.zktravel.model.po.PlCategory;
import com.zktravel.model.vo.category.PlCategoryVO;
import com.zktravel.util.RedisCacheUtil;

@Service
public class PlCategoryServiceImpl implements PlCategoryService {

    @Resource
    private RedisCacheUtil<Object> redisCache;
    @Resource
    private PlCategoryMapper plCategoryMapper;
    @Resource
    private TaProductMapper taProductMapper;

    //获取类目列表
    @Override
    public List<PlCategoryVO> list() {
        List<PlCategoryVO> list = redisCache.getCacheList(CacheConstant.CATEGORY_LIST);
        if (CollectionUtils.isNotEmpty(list)) {
            return list;
        }

        List<PlCategoryVO> voList = getAllCategory(null);

        redisCache.setCacheList(CacheConstant.CATEGORY_LIST, voList);

        return voList;
    }

    @Override
    public Map<Long, PlCategoryVO> getCateMap() {
        Map<Long, PlCategoryVO> map = redisCache.getCacheLongMap(CacheConstant.CATEGORY_MAP);
        if (MapUtils.isNotEmpty(map)) {
            return map;
        }

        List<PlCategoryVO> categoryList = getAllCategory(null);

        Map<Long, PlCategoryVO> categoryMap = categoryList.stream().collect(Collectors.toMap(PlCategoryVO::getId, Function.identity()));

        redisCache.setCacheLongMap(CacheConstant.CATEGORY_MAP, categoryMap);

        return categoryMap;
    }

    //获取全部类目
    @Override
    public List<PlCategoryVO> getAllCategory(List<Long> categoryIds) {
        List<Long> myCategory = new ArrayList<>();
        myCategory.add((long) 4);
        myCategory.add((long) 3);
        myCategory.add((long) 2);
        myCategory.add((long) 1);
        Example ex = new Example(PlCategory.class);
        ex.orderBy("sort").asc().orderBy("id").desc();
        Criteria cr = ex.createCriteria().andIsNull("deletedAt").andNotIn("id", myCategory);
        if (CollectionUtils.isNotEmpty(categoryIds)) {
            cr.andIn("id", categoryIds);
        }
        List<PlCategory> categoryList = plCategoryMapper.selectByExample(ex);

        return BeanUtils.copyList(categoryList, PlCategoryVO.class);
    }

    //下架商品列表
    @Override
    public List<PlCategoryVO> listOff(Long shopId) {
        List<PlCategoryVO> list = null;
        List<Long> categoryIds = taProductMapper.getCategoryIdListOff(shopId);
        if (CollectionUtils.isNotEmpty(categoryIds)) {
            list = getAllCategory(categoryIds);
        }
        return list;
    }
}
