package com.zktravel.service.category;

import java.util.List;
import java.util.Map;

import com.zktravel.model.vo.category.PlCategoryVO;


public interface PlCategoryService {

    List<PlCategoryVO> list();

    List<PlCategoryVO> getAllCategory(List<Long> categoryIds);

    Map<Long, PlCategoryVO> getCateMap();

    List<PlCategoryVO> listOff(Long shopId);
}
