package com.zktravel.service.user;

import com.zktravel.model.dto.user.UserInfo;


public interface UserService {
    UserInfo getUserInfo(Long userId);
    UserInfo getUserInfoByShopId(Long shopId);
}
