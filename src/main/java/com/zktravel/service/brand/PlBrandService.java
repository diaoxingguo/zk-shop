package com.zktravel.service.brand;

import com.odianyun.util.db.query.PageVO;
import com.zktravel.model.dto.brand.PlBrandDTO;
import com.zktravel.model.dto.brand.BrandItem;
import com.zktravel.model.vo.category.PlCategoryVO;

import java.util.List;

public interface PlBrandService {
    PageVO<BrandItem> getBrandList(Integer pageNo, Integer pageSize, Long shopId);

    List<PlCategoryVO> brandCategoryList(PlBrandDTO dto, Long shopId);
}
