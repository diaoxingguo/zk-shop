package com.zktravel.service.brand;

import com.odianyun.util.BeanUtils;
import com.odianyun.util.db.query.PageVO;
import com.zktravel.mapper.PlBrandMapper;
import com.zktravel.mapper.PlProductMapper;
import com.zktravel.model.dto.brand.PlBrandDTO;
import com.zktravel.model.dto.brand.BrandItem;
import com.zktravel.model.vo.brand.PlBrandVO;
import com.zktravel.model.vo.category.PlCategoryVO;
import com.zktravel.service.BaseServiceImpl;
import com.zktravel.service.category.PlCategoryService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class PlBrandServiceImpl extends BaseServiceImpl implements PlBrandService {

    @Resource
    private PlBrandMapper plBrandMapper;
    @Resource
    private PlProductMapper plProductMapper;
    @Resource
    private PlCategoryService plCategoryService;

    @Override
    public PageVO<BrandItem> getBrandList(Integer pageNo, Integer pageSize, Long shopId) {
        // TODO 暂时不开放分页
        pageNo = 0;
        pageSize = 100;

        List<PlBrandVO> list = this.plBrandMapper.list(shopId, pageNo, pageSize);

        List<BrandItem> rstList = new ArrayList<>();
        for (PlBrandVO bTpl : list) {
            BrandItem bItem = BeanUtils.copyProperties(bTpl, BrandItem.class);
            if (999 != bTpl.getSort()) {
                bItem.setTop(1);
            }
            rstList.add(bItem);
        }
        return new PageVO<>(list.size(), rstList);
    }

    @Override
    public List<PlCategoryVO> brandCategoryList(PlBrandDTO dto, Long shopId) {
        List<Long> cateList = plProductMapper.listCategoryByBrandId(dto.getId(), shopId);

        List<PlCategoryVO> list = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(cateList)) {
            Map<Long, PlCategoryVO> map = plCategoryService.getCateMap();
            for (Long id : cateList) {
                if (null != map.get(id)) {
                    list.add(map.get(id));
                }
            }
        }
        return list;
    }

//    /**
//     * 获取品牌列表
//     */
//    @SuppressWarnings("rawtypes")
//    @Override
//    public PageVO<BrandItem> listBrandPage(PlBrandDTO dto, Long shopId) {
//
//        TaProductTopDTO topDTO = new TaProductTopDTO();
//        topDTO.setShopId(shopId);
//
//        List<BrandItem> bList;
//        // 如果是第一页，获取置顶数据
//        if (dto.getPageNo() == ShopConstant.PAGE_FIRST) {
//            List<PlBrandVO> plBrandList = this.handleBrandList(dto.getPageSize(), topDTO);
//
//            PlBrand po = new PlBrand();
//            po.setIsDeleted(0);
//            int count = this.plBrandMapper.selectCount(po);
//            bList = BeanUtils.copyList(plBrandList, BrandItem.class);
//
//            return new PageVO<>(count, bList);
//        }
//
//        topDTO.setType(ShopConstant.TYPE_BRAND);
//        List<TaProductTopVO> topList = this.taProductTopMapper.getListByShopId(topDTO);
//        List<Long> idList = topList.stream().map(TaProductTopVO::getBrandId).collect(Collectors.toList());
//        Example ex = new Example(PlBrand.class);
//        ex.orderBy("id").desc();
//        ex.createCriteria().andEqualTo("status", CommonConstant.AVAILABLE).andEqualTo("isDeleted", CommonConstant.NO);
//        // 如果不是第一页
//        if (CollectionUtils.isNotEmpty(idList)) {
//            ex.createCriteria().andNotIn("id", idList);
//        }
//        PageHelper.offsetPage(dto.getPageNo(), dto.getPageSize());
//        List<PlBrand> poList = this.plBrandMapper.selectByExample(ex);
//        bList = BeanUtils.copyList(poList, BrandItem.class);
//
//        return new PageVO<>(((Page) poList).getTotal(), bList);
//    }

//    /**
//     * 获取品牌推荐
//     *
//     * @param maxNum Integer
//     * @param topDTO TaProductTopDTO
//     */
//    private List<PlBrandVO> handleBrandList(Integer maxNum, TaProductTopDTO topDTO) {
//        List<PlBrandVO> plBrandList = new ArrayList<>();
//
//        // 获取品牌 置顶数据
//        topDTO.setType(ShopConstant.TYPE_BRAND);
//        List<TaProductTopVO> topList = this.taProductTopMapper.getListByShopId(topDTO);
//        Integer limit = maxNum;
//        List<Long> idList = null;
//
//        // 先获取置顶品牌
//        if (CollectionUtils.isNotEmpty(topList)) {
//            Example ex = new Example(PlBrand.class);
//            ex.orderBy("priority").asc();
//            idList = topList.stream().map(TaProductTopVO::getBrandId).collect(Collectors.toList());
//            ex.createCriteria().andEqualTo("status", CommonConstant.AVAILABLE)
//                    .andEqualTo("isDeleted", CommonConstant.NO).andIn("id", idList);
//            List<PlBrand> brandTopList = this.plBrandMapper.selectByExample(ex);
//
//            limit = maxNum - brandTopList.size();
//            List<PlBrandVO> tempVOList = BeanUtils.copyList(brandTopList, PlBrandVO.class);
//            Map<Long, PlBrandVO> map = tempVOList.stream().collect(Collectors.toMap(PlBrandVO::getId, Functions.identity()));
//            List<PlBrandVO> sortTopBrandList = Lists.newArrayList();
//            for (TaProductTopVO vo : topList) {
//                if (null != map.get(vo.getBrandId())) {
//                    PlBrandVO brand = map.get(vo.getBrandId());
//                    // 设置置顶标识
//                    brand.setTop(CommonConstant.TOP);
//                    sortTopBrandList.add(brand);
//                }
//            }
//            plBrandList.addAll(sortTopBrandList);
//        }
//        if (limit > 0) {
//            Example ex = new Example(PlBrand.class);
//            ex.orderBy("priority").asc();
//            Example.Criteria criteria = ex.createCriteria().andEqualTo("status", CommonConstant.AVAILABLE)
//                    .andEqualTo("isDeleted", CommonConstant.NO);
//
//            if (CollectionUtils.isNotEmpty(idList)) {
//                criteria.andNotIn("id", idList);
//            }
//
//            PageHelper.startPage(ProductConstant.PAGENO_1, limit);
//            List<PlBrand> brandList = plBrandMapper.selectByExample(ex);
//            plBrandList.addAll(BeanUtils.copyList(brandList, PlBrandVO.class));
//        }
//
//        return plBrandList;
//    }
}
