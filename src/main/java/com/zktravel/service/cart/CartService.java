package com.zktravel.service.cart;

import com.zktravel.core.vo.ObjectResult;
import com.zktravel.model.dto.cart.CartDTO;
import com.zktravel.model.vo.cart.CartExtVO;
import com.zktravel.model.vo.cart.CartVO;

public interface CartService {

	Integer add(CartDTO dto, Long shopId, Long userId, Integer platformType);

    Integer getCartNum(Long userId, Long shopId, Integer platformType);

    Integer deleteWithTx(CartDTO dto);

    CartVO updateWithTx(CartDTO dto, Long shopId, Integer platformType);

    ObjectResult<CartExtVO> listShop(Long userId, Long shopId);

	ObjectResult<CartExtVO> listShopClient(Long userId, Long shopId);
}
