package com.zktravel.service;

import com.zktravel.mapper.ShopMapper;
import com.zktravel.model.dto.user.UserInfo;
import com.zktravel.model.po.Shop;
import com.zktravel.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

public abstract class BaseServiceImpl {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private UserService uService;
    @Resource
    protected ShopMapper shopMapper;

    protected UserInfo getUser(Long userId) {
        return this.uService.getUserInfo(userId);
    }
    
    protected UserInfo getUserInfoByShopId(Long shopId) {
        return this.uService.getUserInfoByShopId(shopId);
    }
    
    protected Shop getShopInfo(Long sid) {
        Shop shopPO = new Shop();
        shopPO.setId(sid);

        return this.shopMapper.selectOne(shopPO);
    }
}
