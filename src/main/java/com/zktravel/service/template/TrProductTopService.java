package com.zktravel.service.template;

import java.util.List;

import com.zktravel.model.dto.template.TaProductTopDTO;

public interface TrProductTopService {

    void goTop(TaProductTopDTO dto, Long shopId, Long userId);

    Integer topNum(TaProductTopDTO dto, Long shopId);

    void cancelTop(TaProductTopDTO dto, Long shopId, Long userId);

    /**
     * 根据 数量 和 分类 将过多的置顶删除
     *
     * @param categoryId 类型
     * @param num        置顶个数
     */
    void cancelTopBySort(Long categoryId, Integer num, Long shopId, Long userId);

    void sort(Long shopId, Long categoryId, Integer sortIndex, List<Long> ids);

    Integer getMaxTopNum(Long catId, Long shopId);
}
