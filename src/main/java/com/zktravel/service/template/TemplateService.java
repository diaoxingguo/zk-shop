package com.zktravel.service.template;

import com.odianyun.util.db.query.PageVO;
import com.zktravel.model.dto.category.CategoryGroup;
import com.zktravel.model.dto.product.ProductItem2B;
import com.zktravel.model.dto.product.ProductItem2C;
import com.zktravel.model.dto.template.Template;
import com.zktravel.model.vo.product.PdQueryParams;

import java.util.List;
import java.util.Map;


public interface TemplateService {
    Map<String, Object> getShopTemplate2B(Long shopId, Long userId);

    Map<String, Object> getShopTemplate2C(Long shopId);

    PageVO<ProductItem2B> getProductList(PdQueryParams qry);
    PageVO<ProductItem2C> getProductList2C(PdQueryParams qry);

    List<CategoryGroup> getCategoryGroup(Integer catId, Long shopId, Long userId);

    List<Template> getShopTemplate(Long shopId);

    void updateShopTemplate(List<Template> tplList, Long shopId);
}
