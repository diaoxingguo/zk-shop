package com.zktravel.service.message;

import com.alibaba.fastjson.JSONObject;

public interface MessageSendService {

	JSONObject postRequest(String uri, String jsonParams);
	
}
