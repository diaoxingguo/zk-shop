package com.zktravel.service.message;

import java.math.BigDecimal;
import java.util.List;

public interface WxMsgService {

	 void sendOrderMsgToAdmin(String shopName, String userName, String mpName, BigDecimal totalAmt);
	 void sendOrderMsgToAdminWithUserId(String shopName, Long userId, String mpName, BigDecimal totalAmt);
	 void sendOrderMsgToTailor(List<String> openIds, Long userId, String mpName, BigDecimal profitAmt, Long orderId,String remark);
	 void sendCartMsgToTailor(Long shopId, Long userId, String mpName, Long tpid);
}
