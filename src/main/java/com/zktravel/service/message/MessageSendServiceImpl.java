package com.zktravel.service.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.zktravel.util.HttpUtils;

@Service
public class MessageSendServiceImpl implements MessageSendService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageSendServiceImpl.class);

    @Value("#{configProperties['access.domain']}")
    private String domain;

    
    /**
     * @Title: postRequest
     * @Description: 发送请求
     * @param uri
     * @param jsonParams
     * @return 参数说明
     * @return 返回类型
     * @throws 
     * @author diaoxingguo
     * @date 2018年6月23日
     */
	@Override
	public JSONObject postRequest(String uri, String jsonParams) {
		String url = this.domain + uri;
		
		LOGGER.info("请求外部接口,URl:{},jsonParams:{}", url, jsonParams);
		
        String dataResult = HttpUtils.sendPostJsonRequest(url, jsonParams);
        
        LOGGER.info("请求外部接口{}返回值：{}", url, dataResult);

        return JSONObject.parseObject(dataResult);
	}
    
}
