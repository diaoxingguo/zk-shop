package com.zktravel.service.order;

import java.util.List;
import java.util.Map;

import com.zktravel.model.dto.coupon.CusCouponDTO;
import com.zktravel.model.dto.coupon.CouListResponse;
import com.zktravel.model.po.CusCoupon;

public interface CusCouponService {

    void receiveCoupon(CusCouponDTO dto, Long shopId, Long userId);

    Map<Long, CusCoupon> getMapByTpids(List<Long> tpidList, Long userId, Long shopId);

    List<CouListResponse> getList(Long userId, Integer type, Integer page, Integer pageSize);

    CusCoupon exist(Long tpid, Long shopId, Long userId);
}
