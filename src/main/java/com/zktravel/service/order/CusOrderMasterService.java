package com.zktravel.service.order;

import com.zktravel.model.dto.order.CusOrderMasterDTO;
import com.zktravel.model.vo.order.CusOrderMasterTableVO;

import java.util.Map;

public interface CusOrderMasterService {

    Integer checkOrderStatus(Long userId, Long orderId);

    Map<String, Object> getPaymentInfoTailor(CusOrderMasterDTO dto, Long shopId, Long userId);

    CusOrderMasterTableVO submitOrderTailorWithTx(CusOrderMasterDTO dto, Long shopId, Long userId);

    Map<String, Object> getPaymentInfoClient(CusOrderMasterDTO dto, Long shopId, Long userId);

    CusOrderMasterTableVO submitOrderClientWithTx(CusOrderMasterDTO dto, Long shopId, Long userId);

}
