package com.zktravel.service.product;

import java.util.List;

import com.odianyun.util.db.query.PageVO;
import com.zktravel.model.dto.product.PlProductDTO;
import com.zktravel.model.dto.product.ProductItem2C;
import com.zktravel.model.dto.product.TaProductDTO;
import com.zktravel.model.vo.common.SimpleDataVO;
import com.zktravel.model.vo.product.PlProductVO;


public interface PlProductService {

    PageVO<PlProductVO> listPage(PlProductDTO dto, Long userId);

    PlProductVO updatePrice(TaProductDTO dto, Long shopId);

    void updateStatus(TaProductDTO dto, Long shopId);

    PlProductVO getInfo(PlProductDTO dto, Long shopId, Integer platform);

    PageVO<PlProductVO> togeListPage(PlProductDTO dto, Integer platformType);

	List<SimpleDataVO> inklingQuery(String name, Long shopId);

	PageVO<ProductItem2C> listPageClient(PlProductDTO dto);

}
