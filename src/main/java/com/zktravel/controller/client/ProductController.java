package com.zktravel.controller.client;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.odianyun.util.db.query.PageVO;
import com.zktravel.constants.CommonConstant;
import com.zktravel.controller.BaseController;
import com.zktravel.core.exception.VisibleException;
import com.zktravel.core.vo.ListResult;
import com.zktravel.core.vo.ObjectResult;
import com.zktravel.core.vo.PageResult;
import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.product.PlProductDTO;
import com.zktravel.model.dto.product.ProductItem2C;
import com.zktravel.model.dto.template.ReqIndex;
import com.zktravel.model.vo.common.SimpleDataVO;
import com.zktravel.model.vo.product.PdQueryParams;
import com.zktravel.model.vo.product.PlProductVO;
import com.zktravel.service.product.PlProductService;
import com.zktravel.service.template.TemplateService;

@Api(description = "商品C端")
@Controller("cli-product")
@RequestMapping("/cli-product")
@RestController
public class ProductController extends BaseController {

    @Resource
    private TemplateService shopService;
    @Resource
    private PlProductService plProductService;

    @PostMapping("/get-shop-template")
    public ObjectResult<Map<String, Object>> getShopIndex(@RequestBody ReqIndex req) {
        Long shopId = req.getSid();

        Map<String, Object> map = this.shopService.getShopTemplate2C(shopId);

        return ObjectResult.ok(map);
    }

    @ApiOperation(value = "商品详情(C端)")
    @PostMapping("/get-goods-detail")
    public ObjectResult<PlProductVO> getInfo(@RequestBody PlProductDTO dto) {
        if (null == dto || null == dto.getId()) {
            throw new VisibleException(CodeEnum.ID_NOT_NULL);
        }

        Long shopId = dto.getSid();
        PlProductVO vo = this.plProductService.getInfo(dto, shopId, CommonConstant.PLATFORM_TYPE_C);
        return ObjectResult.ok(vo);
    }

    @ApiOperation(value = "C端拼凑商品列表")
    @PostMapping("/get-toge-list")
    public PageResult<PlProductVO> togeListPage(@RequestBody PlProductDTO dto) {
        dto.setShopId(dto.getSid());
        PageVO<PlProductVO> list = plProductService.togeListPage(dto, CommonConstant.PLATFORM_TYPE_C);
        
        return PageResult.ok(list);
    }


    @ApiOperation(value = "店铺商品分页查询")
    @PostMapping("/get-product-list")
    public PageResult<ProductItem2C> listProductPage(@RequestBody PlProductDTO dto) {
        Long sId = dto.getSid();

        PdQueryParams qry = new PdQueryParams();
        qry.setShopId(sId);
        Long catId = dto.getCategoryId();
        Long brandId = dto.getBrandId();
        if (null != catId && catId > (long) 0) {
            qry.setCategoryId(catId);
        }
        if (null != brandId && brandId > (long) 0) {
            qry.setBrandId(brandId);
        }
        qry.setTopType(dto.getType());
        qry.setOrderBy(dto.getOrderBy());
        qry.setPageNo(dto.getPageNo());
        qry.setPageSize(dto.getPageSize());

        PageVO<ProductItem2C> vo = this.shopService.getProductList2C(qry);

        return PageResult.ok(vo);
    }
    
    @ApiOperation(value = "商品分页查询")
    @PostMapping("/get-list")
    public PageResult<ProductItem2C> listPage(@RequestBody PlProductDTO dto) {
         dto.setShopId(dto.getSid());
         PageVO<ProductItem2C> list = plProductService.listPageClient(dto);
         return PageResult.ok(list);
    }
    
    
    @ApiOperation(value = "商品名模糊查询")
    @PostMapping("/inkling-query")
    public ListResult<SimpleDataVO> inklingQuery(@RequestBody PlProductDTO dto){
    	if(null == dto || StringUtils.isBlank(dto.getName())){
    		return ListResult.ok(Lists.newArrayList());
    	}
    	
    	Long shopId = dto.getSid();
    	List<SimpleDataVO> list = plProductService.inklingQuery(dto.getName(), shopId);
    	
		return ListResult.ok(list);
    }
    
    @ApiOperation(value = "商品名缓存查询")
    @PostMapping("/cache-query")
    public ListResult<SimpleDataVO> cacheQuery(@RequestBody PlProductDTO dto){
		return ListResult.ok(Lists.newArrayList());
    }
}
