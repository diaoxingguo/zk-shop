package com.zktravel.controller.client;

import com.zktravel.controller.BaseController;
import com.zktravel.core.exception.VisibleException;
import com.zktravel.core.session.SessionContext;
import com.zktravel.core.vo.ObjectResult;
import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.order.CusOrderMasterDTO;
import com.zktravel.model.dto.order.OrderCheckDTO;
import com.zktravel.model.vo.order.CusOrderMasterTableVO;
import com.zktravel.service.order.CusOrderMasterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(description = "c端：订单")
@Controller("cli-order")
@RestController
public class OrderController extends BaseController {
    @Resource
    private CusOrderMasterService cusOrderMasterService;

    @ApiOperation(value = "结算，订单预览C端")
    @PostMapping("/cli-order/get-payment-info")
    public ObjectResult<Map<String, Object>> getPaymentInfoClient(@RequestBody CusOrderMasterDTO dto) {
        if (null == dto || CollectionUtils.isEmpty(dto.getCartIds())) {
            throw new VisibleException(CodeEnum.CART_ID_NOT_NULL);
        }
        Long shopId = dto.getSid();
        Long userId = SessionContext.getUserId();
        Map<String, Object> map = this.cusOrderMasterService.getPaymentInfoClient(dto, shopId, userId);

        return ObjectResult.ok(map);
    }

    @ApiOperation(value = "订单提交C端")
    @PostMapping("/cli-order/submit-order")
    public ObjectResult<CusOrderMasterTableVO> submitOrderClient(@RequestBody CusOrderMasterDTO dto) {
        CodeEnum code = dto.validSubmit();
        if (null != code) {
            throw new VisibleException(code);
        }
        Long shopId = dto.getSid();
        Long userId = SessionContext.getUserId();
        CusOrderMasterTableVO vo = this.cusOrderMasterService.submitOrderClientWithTx(dto, shopId, userId);

        return ObjectResult.ok(vo);
    }

    @ApiOperation(value = "检查订单状态")
    @PostMapping("/cli-order/check-order")
    public ObjectResult<Integer> checkOrder(@RequestBody OrderCheckDTO dto) {
        // go 转发问题导致该接口从 post参数里面取
        Long userId = (long) dto.getUserId();
//        Long userId = SessionContext.getUserId();
        Long orderId = dto.getOrderId();
        Integer resultCode = this.cusOrderMasterService.checkOrderStatus(userId, orderId);

        return ObjectResult.ok(resultCode);
    }
}
