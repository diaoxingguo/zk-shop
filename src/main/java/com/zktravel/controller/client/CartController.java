package com.zktravel.controller.client;

import com.zktravel.constants.CommonConstant;
import com.zktravel.controller.BaseController;
import com.zktravel.core.exception.VisibleException;
import com.zktravel.core.session.SessionContext;
import com.zktravel.core.vo.ObjectResult;
import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.cart.CartDTO;
import com.zktravel.model.vo.cart.CartExtVO;
import com.zktravel.model.vo.cart.CartVO;
import com.zktravel.service.cart.CartService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(description = "c端：购物车")
@Controller("cli-cart")
@RestController
@RequestMapping("cli-cart")
public class CartController extends BaseController {
    @Resource
    private CartService cartService;

    @ApiOperation(value = "C端:加入购物车")
    @PostMapping("add")
    public ObjectResult<Integer> addClient(@RequestBody CartDTO dto) {
        // dto 中需要传 sid
        Long userId = SessionContext.getUserId();
        Long shopId = dto.getSid();
        CodeEnum errCode = dto.validate();
        if (null != errCode) {
            throw new VisibleException(errCode);
        }
        dto.setUid(userId);

        Integer num = this.cartService.add(dto, shopId, userId, CommonConstant.PLATFORM_TYPE_C);
        return ObjectResult.ok(num);
    }

    @ApiOperation(value = "C端:设置购物车商品数量")
    @PostMapping("update")
    public ObjectResult<CartVO> updateClient(@RequestBody CartDTO dto) {
        // dto 中需要传 sid
        CodeEnum code = dto.validateId();
        if (null != code) {
            throw new VisibleException(code);
        }
        if (null == dto.getNum() || dto.getNum() <= 0) {
            throw new VisibleException(CodeEnum.PRODUCT_NUM_GT_0);
        }
        Long userId = SessionContext.getUserId();
        Long shopId = dto.getSid();
        dto.setUid(userId);
        CartVO vo = this.cartService.updateWithTx(dto, shopId, CommonConstant.PLATFORM_TYPE_C);
        return ObjectResult.ok(vo);
    }

    @ApiOperation(value = "C端:删除购物车商品")
    @PostMapping("delete")
    public ObjectResult<Integer> deleteClient(@RequestBody CartDTO dto) {
        // dto 中需要传 sid
        Long userId = SessionContext.getUserId();
        dto.setUid(userId);
        CodeEnum code = dto.validateId();
        if (null != code) {
            throw new VisibleException(code);
        }
        dto.setPlatformType(CommonConstant.PLATFORM_TYPE_C);
        Integer num = this.cartService.deleteWithTx(dto);
        return ObjectResult.ok(num);
    }

    @ApiOperation(value = "C端:获取购物车商品数")
    @PostMapping("num")
    public ObjectResult<Integer> numClient(@RequestBody CartDTO dto) {
        Long userId = SessionContext.getUserId();
        Integer num = this.cartService.getCartNum(userId, dto.getSid(), CommonConstant.PLATFORM_TYPE_C);
        return ObjectResult.ok(num);
    }

    @ApiOperation(value = "商家购物车列表C端")
    @PostMapping("get-list")
    public ObjectResult<CartExtVO> listShopClient(@RequestBody CartDTO dto) {
        // dto 中需要传 sid
        Long userId = SessionContext.getUserId();
        return this.cartService.listShopClient(userId, dto.getSid());
    }
}
