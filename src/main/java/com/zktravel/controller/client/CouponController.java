package com.zktravel.controller.client;

import com.zktravel.controller.BaseController;
import com.zktravel.core.exception.VisibleException;
import com.zktravel.core.session.SessionContext;
import com.zktravel.core.vo.ListResult;
import com.zktravel.core.vo.Result;
import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.coupon.CouListRequest;
import com.zktravel.model.dto.coupon.CouListResponse;
import com.zktravel.model.dto.coupon.CusCouponDTO;
import com.zktravel.service.order.CusCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api(description = "优惠券")
@RestController("cli-coupon")
public class CouponController extends BaseController {

    @Resource
    private CusCouponService cusCouponService;

    @ApiOperation(value = "领取优惠券")
    @PostMapping("/cli-coupon/receive-coupon")
    public Result receiveCoupon(@RequestBody CusCouponDTO dto) {
        if (null == dto.getGoods_id()) {
            throw new VisibleException(CodeEnum.PRODUCT_ID_NOT_NULL);
        }
        Long shopId = dto.getSid();
        Long userId = SessionContext.getUserId();

        this.cusCouponService.receiveCoupon(dto, shopId, userId);

        return Result.OK;
    }

    @ApiOperation(value = "优惠券列表")
    @PostMapping("/cli-coupon/list")
    public Result Coupon(@RequestBody CouListRequest dto) {
        Long userId = SessionContext.getUserId();
        if (null == dto.getType()) {
            throw new VisibleException(CodeEnum.TYPE_MISSING);
        }

        List<CouListResponse> result = this.cusCouponService.getList(
                userId, dto.getType(), dto.getPageNo(), dto.getPageSize()
        );
        return ListResult.ok(result);
    }
    
}
