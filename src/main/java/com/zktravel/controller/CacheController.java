package com.zktravel.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zktravel.core.vo.ObjectResult;
import com.zktravel.util.RedisCacheUtil;

@Controller
@RequestMapping("/cache")
public class CacheController {

    @Resource
    private RedisCacheUtil redisCacheUtil;
	
	/*@RequestMapping("/clearPrefix")
    @ResponseBody
    public ObjectResult clearCache(@RequestParam("prefix") String prefix) {
		redisCacheUtil.removePrefix(prefix);
    	return ObjectResult.ok(null);
    }*/

    @RequestMapping("/clear")
    @ResponseBody
    public ObjectResult clearCache(@RequestParam("key") String key) {
        redisCacheUtil.remove(key);

        return ObjectResult.ok(null);
    }
}
