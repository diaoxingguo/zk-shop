package com.zktravel.controller.tailor;

import com.odianyun.util.db.query.PageVO;
import com.zktravel.constants.ShopConstant;
import com.zktravel.controller.BaseController;
import com.zktravel.core.session.SessionContext;
import com.zktravel.core.vo.ListResult;
import com.zktravel.core.vo.ObjectResult;
import com.zktravel.core.vo.PageResult;
import com.zktravel.core.vo.Result;
import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.product.PlProductDTO;
import com.zktravel.model.dto.category.CategoryGroup;
import com.zktravel.model.dto.category.CategoryReq;
import com.zktravel.model.dto.product.ProductItem2B;
import com.zktravel.model.dto.template.TemplateList;
import com.zktravel.model.vo.product.PdQueryParams;
import com.zktravel.service.template.TemplateService;
import com.zktravel.service.template.TrProductTopService;
import com.zktravel.service.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Api(description = "店铺")
@RequestMapping("/tr-template")
@RestController
public class TemplateController extends BaseController {
    @Resource
    private TemplateService shopService;
    @Resource
    private TrProductTopService taProductTopService;
    @Resource
    private UserService uService;

    /**
     * B端获取店铺模板设置
     */
    @ApiOperation(value = "获取用户模板（B端）")
    @PostMapping("/get-template-list")
    public ObjectResult<Map<String, Object>> getShopTemplate() {
        Long shopId = SessionContext.getShopId();
        Long userId = SessionContext.getUserId();
        Map<String, Object> map = this.shopService.getShopTemplate2B(shopId, userId);

        return ObjectResult.ok(map);
    }

    @ApiOperation(value = "修改用户模板", notes = "参数:{\"template\":[{\"type\":1,\"num\":4}]}")
    @PostMapping("/update")
    public Result updateShopTemplate(@RequestBody TemplateList dto) {
        if (CollectionUtils.isEmpty(dto.getTemplate())) {
            return new Result(CodeEnum.SHOP_TEMPLATE_NOT_NULL);
        }
        Long shopId = SessionContext.getShopId();
        Long userId = SessionContext.getUserId();

        this.shopService.updateShopTemplate(dto.getTemplate(), shopId);
        // 如果模板由多数改为少数，需要去掉多余的置顶
        if (null != dto.getModify()) {
            this.taProductTopService.cancelTopBySort(
                    dto.getModify().getType().longValue(),
                    dto.getModify().getNum(),
                    shopId,
                    userId
            );
        }
        return new Result(CodeEnum.OK);
    }


    @ApiOperation(value = "店铺类型类目查询", notes = "参数:{\"type\":0}")
    @PostMapping("/get-category-group")
    public ListResult<CategoryGroup> listTypeCategory(@RequestBody CategoryReq dto) {
        if (null == dto.getType()) {
            dto.setType(ShopConstant.TYPE_EUROPE_BUY);
        }
        Long shopId = SessionContext.getShopId();
        Long userId = SessionContext.getUserId();
        List<CategoryGroup> list = this.shopService.getCategoryGroup(dto.getType(), shopId, userId);

        return ListResult.ok(list);
    }

    @ApiOperation(value = "店铺商品分页查询(含置顶)")
    @PostMapping("/get-product-list")
    public PageResult<ProductItem2B> listProductPage(@RequestBody PlProductDTO dto) {
        Long sId = SessionContext.getShopId();
        Long uId = SessionContext.getUserId();

        PdQueryParams qry = new PdQueryParams();
        qry.setShopId(sId);
        Long catId = dto.getCategoryId();
        Long brandId = dto.getBrandId();
        if (null != catId && catId > (long) 0) {
            qry.setCategoryId(catId);
        }
        if (null != brandId && brandId > (long) 0) {
            qry.setBrandId(brandId);
        }
        qry.setTopType(dto.getType());
        qry.setOrderBy(dto.getOrderBy());
        qry.setPageNo(dto.getPageNo());
        qry.setPageSize(dto.getPageSize());
        qry.setVip(this.uService.getUserInfo(uId).isVip());

        PageVO<ProductItem2B> vo = this.shopService.getProductList(qry);

        return PageResult.ok(vo);
    }
}
