package com.zktravel.controller.tailor;

import com.zktravel.controller.BaseController;
import com.zktravel.core.session.SessionContext;
import com.zktravel.model.dto.template.TopSort;
import com.zktravel.core.exception.VisibleException;
import com.zktravel.core.vo.ObjectResult;
import com.zktravel.core.vo.Result;
import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.template.TaProductTopDTO;
import com.zktravel.service.template.TrProductTopService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(description = "商品置顶")
@RequestMapping("/tr-product-top")
@RestController
public class ProductTopController extends BaseController {
    @Resource
    private TrProductTopService taProductTopService;

    @ApiOperation(value = "置顶", notes = "参数(类型type为0时brandId必传，其他productId必传):{\"type\":0,\"brandId\":0,\"productId\":0,\"sort\":0}")
    @PostMapping("/set-top")
    public Result setTop(@RequestBody TaProductTopDTO dto) {
        // 需要传 type | productId | brandId | sort
        CodeEnum code = dto.validTop();
        if (null != code) {
            return new Result(code);
        }
        Long userId = SessionContext.getUserId();
        Long shopId = SessionContext.getShopId();

        this.taProductTopService.goTop(dto, shopId, userId);
        return new Result(CodeEnum.OK);
    }

    @ApiOperation(value = "取消置顶", notes = "参数:{\"id\":1}")
    @PostMapping("/cancel-top")
    public Result cancelTop(@RequestBody TaProductTopDTO dto) {
        if (null == dto.getItemId()) {
            throw new VisibleException(CodeEnum.BRAND_PRODUCT_NOT_NULL);
        }
        Long shopId = SessionContext.getShopId();
        Long userId = SessionContext.getUserId();
        taProductTopService.cancelTop(dto, shopId, userId);
        return new Result(CodeEnum.OK);
    }

    @ApiOperation(value = "获取模板置顶数量", notes = "参数:{\"type\":1}")
    @PostMapping("/get-num")
    public ObjectResult<Integer> templateNum(@RequestBody TaProductTopDTO dto) {
        if (null == dto || null == dto.getCategoryId()) {
            throw new VisibleException(CodeEnum.PRODUCT_TOP_TYPE_NOT_NULL);
        }
        Long shopId = SessionContext.getShopId();
        Integer num = taProductTopService.topNum(dto, shopId);
        return ObjectResult.ok(num);
    }

    @ApiOperation(value = "获取置顶数量", notes = "参数:{\"type\":1}")
    @PostMapping("/get-top-num")
    public ObjectResult<Integer> getTopNum(@RequestBody TaProductTopDTO dto) {
        if (null == dto || null == dto.getCategoryId()) {
            throw new VisibleException(CodeEnum.PRODUCT_TOP_TYPE_NOT_NULL);
        }

        Long shopId = SessionContext.getShopId();
        Integer num = this.taProductTopService.getMaxTopNum(dto.getCategoryId(), shopId);
        return ObjectResult.ok(num);
    }

    @ApiOperation(value = "置顶排序", notes = "参数:{[\"list\":{\"id\":1,\"sort\":1}]}")
    @PostMapping("/sort")
    public Result sort(@RequestBody TopSort dto) {
        // 逻辑为 前台给 type 和 id的顺序 后台直接 修改
        Long shopId = SessionContext.getShopId();
        if (null == dto || CollectionUtils.isEmpty(dto.getIds())) {
            return new Result(CodeEnum.PRODUCT_LIST_NOT_NULL);
        }
        taProductTopService.sort(shopId, dto.getCategoryId(), 1, dto.getIds());
        return new Result(CodeEnum.OK);
    }
}
