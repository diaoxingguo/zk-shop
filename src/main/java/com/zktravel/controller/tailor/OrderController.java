package com.zktravel.controller.tailor;

import com.zktravel.controller.BaseController;
import com.zktravel.core.exception.VisibleException;
import com.zktravel.core.session.SessionContext;
import com.zktravel.core.vo.ObjectResult;
import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.order.CusOrderMasterDTO;
import com.zktravel.model.vo.order.CusOrderMasterTableVO;
import com.zktravel.service.order.CusOrderMasterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(description = "b端:订单")
@Controller("tr-order")
@RestController
public class OrderController extends BaseController {
    @Resource
    private CusOrderMasterService cusOrderMasterService;

    @ApiOperation(value = "结算，订单预览B端")
    @PostMapping("/tr-order/get-payment-info")
    public ObjectResult<Map<String, Object>> getPaymentInfo(@RequestBody CusOrderMasterDTO dto) {
        if (null == dto || CollectionUtils.isEmpty(dto.getCartIds())) {
            throw new VisibleException(CodeEnum.CART_ID_NOT_NULL);
        }
        Long shopId = SessionContext.getShopId();
        Long userId = SessionContext.getUserId();
        Map<String, Object> map = this.cusOrderMasterService.getPaymentInfoTailor(dto, shopId, userId);

        return ObjectResult.ok(map);
    }

    @ApiOperation(value = "订单提交B端")
    @PostMapping("/tr-order/submit-order")
    public ObjectResult<CusOrderMasterTableVO> submitOrder(@RequestBody CusOrderMasterDTO dto) {
        CodeEnum code = dto.validSubmit();
        if (null != code) {
            throw new VisibleException(code);
        }
        Long shopId = SessionContext.getShopId();
        Long userId = SessionContext.getUserId();
        CusOrderMasterTableVO vo = this.cusOrderMasterService.submitOrderTailorWithTx(dto, shopId, userId);

        return ObjectResult.ok(vo);
    }
}
