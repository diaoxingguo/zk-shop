package com.zktravel.controller.tailor;

import com.zktravel.constants.CommonConstant;
import com.zktravel.controller.BaseController;
import com.zktravel.core.exception.VisibleException;
import com.zktravel.core.session.SessionContext;
import com.zktravel.core.vo.ObjectResult;
import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.cart.CartDTO;
import com.zktravel.model.vo.cart.CartExtVO;
import com.zktravel.model.vo.cart.CartVO;
import com.zktravel.service.cart.CartService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(description = "b端:购物车")
@Controller("tr-cart")
@RestController
@RequestMapping("tr-cart")
public class CartController extends BaseController {

    @Resource
    private CartService cartService;

    @ApiOperation(value = "B端:加入购物车")
    @PostMapping("/add")
    public ObjectResult<Integer> add(@RequestBody CartDTO dto) {
        Long shopId = SessionContext.getShopId();
        Long userId = SessionContext.getUserId();
        CodeEnum errCode = dto.validate();
        if (null != errCode) {
            throw new VisibleException(errCode);
        }
        dto.setUid(userId);
        dto.setSid(shopId);

        Integer num = this.cartService.add(dto, shopId, userId, CommonConstant.PLATFORM_TYPE_B);
        return ObjectResult.ok(num);
    }

    @ApiOperation(value = "B端:设置购物车商品数量")
    @PostMapping("/update")
    public ObjectResult<CartVO> update(@RequestBody CartDTO dto) {
        CodeEnum code = dto.validateId();
        if (null != code) {
            throw new VisibleException(code);
        }
        if (null == dto.getNum() || dto.getNum() <= 0) {
            throw new VisibleException(CodeEnum.PRODUCT_NUM_GT_0);
        }
        Long userId = SessionContext.getUserId();
        Long shopId = SessionContext.getShopId();

        dto.setUid(userId);
        dto.setSid(shopId);
        CartVO vo = this.cartService.updateWithTx(dto, shopId, CommonConstant.PLATFORM_TYPE_B);
        return ObjectResult.ok(vo);
    }

    @ApiOperation(value = "B端:删除购物车商品")
    @PostMapping("/delete")
    public ObjectResult<Integer> delete(@RequestBody CartDTO dto) {
        Long shopId = SessionContext.getShopId();
        Long userId = SessionContext.getUserId();

        dto.setUid(userId);
        dto.setSid(shopId);
        CodeEnum code = dto.validateId();
        if (null != code) {
            throw new VisibleException(code);
        }
        dto.setPlatformType(CommonConstant.PLATFORM_TYPE_B);
        Integer num = this.cartService.deleteWithTx(dto);
        return ObjectResult.ok(num);
    }

    @ApiOperation(value = "B端:获取购物车商品数")
    @PostMapping("/num")
    public ObjectResult<Integer> num() {
        Long userId = SessionContext.getUserId();
        Long shopId = SessionContext.getShopId();
        Integer num = this.cartService.getCartNum(userId, shopId, CommonConstant.PLATFORM_TYPE_B);
        return ObjectResult.ok(num);
    }

    @ApiOperation(value = "商家购物车列表B端")
    @PostMapping("/get-list")
    public ObjectResult<CartExtVO> listShop() {
        Long shopId = SessionContext.getShopId();
        Long userId = SessionContext.getUserId();
        return this.cartService.listShop(userId, shopId);
    }
}
