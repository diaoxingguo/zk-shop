package com.zktravel.controller.tailor;

import com.odianyun.util.db.query.PageVO;
import com.zktravel.controller.BaseController;
import com.zktravel.core.exception.VisibleException;
import com.zktravel.core.session.SessionContext;
import com.zktravel.core.vo.ListResult;
import com.zktravel.core.vo.PageResult;
import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.brand.PlBrandDTO;
import com.zktravel.model.dto.brand.BrandItem;
import com.zktravel.model.vo.category.PlCategoryVO;
import com.zktravel.service.brand.PlBrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api(description = "品牌")
@RequestMapping("/tr-brand")
@RestController
public class BrandController extends BaseController {

    @Resource
    private PlBrandService plBrandService;

    @ApiOperation(value = "店铺品牌分页查询(含置顶)", notes = "参数:{\"pageNo\":0,\"pageSize\":1}")
    @PostMapping("/get-list")
    public PageResult<BrandItem> listBrandPage(@RequestBody PlBrandDTO dto) {
        Long shopId = SessionContext.getShopId();
        PageVO<BrandItem> vo = this.plBrandService.getBrandList(dto.getPageNo(), dto.getPageSize(), shopId);

        return PageResult.ok(vo);
    }

    @ApiOperation(value = "根据品牌获取类目列表")
    @PostMapping("/brand-category-list")
    public ListResult<PlCategoryVO> brandCategoryList(@RequestBody PlBrandDTO dto) {
        CodeEnum code = dto.validateId();
        if (null != code) {
            throw new VisibleException(code);
        }
        Long shopId = SessionContext.getShopId();

        List<PlCategoryVO> list = plBrandService.brandCategoryList(dto, shopId);
        return ListResult.ok(list);
    }
}
