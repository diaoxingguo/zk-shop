package com.zktravel.controller.tailor;

import com.zktravel.controller.BaseController;
import com.zktravel.core.session.SessionContext;
import com.zktravel.core.vo.ListResult;
import com.zktravel.model.vo.category.PlCategoryVO;
import com.zktravel.service.category.PlCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api(description = "类目")
@RequestMapping("/tr-category")
@RestController
public class CategoryController extends BaseController {

    @Resource
    private PlCategoryService plCategoryService;

    @ApiOperation(value = "类目列表(全部)")
    @PostMapping("/get-list")
    public ListResult<PlCategoryVO> list() {
        List<PlCategoryVO> list = plCategoryService.list();
        return ListResult.ok(list);
    }

    @ApiOperation(value = "类目列表（下架商品）")
    @PostMapping("/get-off-list")
    public ListResult<PlCategoryVO> listOff() {
        Long shopId = SessionContext.getShopId();
        List<PlCategoryVO> list = plCategoryService.listOff(shopId);
        return ListResult.ok(list);
    }
}
