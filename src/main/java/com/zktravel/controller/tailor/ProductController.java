package com.zktravel.controller.tailor;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.odianyun.util.db.query.PageVO;
import com.zktravel.constants.CommonConstant;
import com.zktravel.controller.BaseController;
import com.zktravel.core.exception.VisibleException;
import com.zktravel.core.session.SessionContext;
import com.zktravel.core.vo.ListResult;
import com.zktravel.core.vo.ObjectResult;
import com.zktravel.core.vo.PageResult;
import com.zktravel.core.vo.Result;
import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.product.PlProductDTO;
import com.zktravel.model.dto.product.TaProductDTO;
import com.zktravel.model.vo.common.SimpleDataVO;
import com.zktravel.model.vo.product.PlProductVO;
import com.zktravel.service.product.PlProductService;


@Api(description = "商品B端")
@Controller("tr-product")
@RequestMapping("/tr-product")
@RestController
public class ProductController extends BaseController {

    @Resource
    private PlProductService plProductService;

    @ApiOperation(value = "商家商品列表", notes = "参数:brandId品牌id;name:商品名;categoryId:类目id:\n{\"brandId\":1}")
    @PostMapping("/get-list")
    public PageResult<PlProductVO> listPage(@RequestBody PlProductDTO dto) {
        Long userId = SessionContext.getUserId();
        dto.setShopId(SessionContext.getShopId());
        PageVO<PlProductVO> list = this.plProductService.listPage(dto, userId);
        return PageResult.ok(list);
    }

    @ApiOperation(value = "商家拼凑商品列表")
    @PostMapping("/get-toge-list")
    public PageResult<PlProductVO> togeListPage(@RequestBody PlProductDTO dto) {
        dto.setShopId(SessionContext.getShopId());
        PageVO<PlProductVO> list = plProductService.togeListPage(dto, CommonConstant.PLATFORM_TYPE_B);
        return PageResult.ok(list);
    }

    @ApiOperation(value = "商品详情")
    @PostMapping("/get-detail")
    public ObjectResult<PlProductVO> getInfo(@RequestBody PlProductDTO dto) {
        if (null == dto || null == dto.getId()) {
            throw new VisibleException(CodeEnum.ID_NOT_NULL);
        }
        Long shopId = SessionContext.getShopId();
        PlProductVO vo = plProductService.getInfo(dto, shopId, CommonConstant.PLATFORM_TYPE_B);
        return ObjectResult.ok(vo);
    }

    @ApiOperation(value = "调价")
    @PostMapping("/update-price")
    public ObjectResult<PlProductVO> updatePrice(@RequestBody TaProductDTO dto) {
        if (null == dto || null == dto.getId()) {
            throw new VisibleException(CodeEnum.ID_NOT_NULL);
        }

        if (null == dto.getSellPrice() || dto.getSellPrice().compareTo(BigDecimal.ZERO) < 0) {
            throw new VisibleException(CodeEnum.TA_PRODUCT_SELL_PRICE_NOT_NULL);
        }
        Long shopId = SessionContext.getShopId();
        PlProductVO vo = plProductService.updatePrice(dto, shopId);
        
        return ObjectResult.ok(vo);
    }

    @ApiOperation(value = "上下架")
    @PostMapping("/update-status")
    public Result updateStatus(@RequestBody TaProductDTO dto) {
        if (null == dto || null == dto.getId()) {
            throw new VisibleException(CodeEnum.ID_NOT_NULL);
        }

        if (null == dto.getStatus()) {
            throw new VisibleException(CodeEnum.PRODUCT_STATUS_NOT_NULL);
        }
        Long shopId = SessionContext.getShopId();

        plProductService.updateStatus(dto, shopId);

        return new Result(CodeEnum.OK);
    }
    
    @ApiOperation(value = "商品名模糊查询")
    @PostMapping("/inkling-query")
    public ListResult<SimpleDataVO> inklingQuery(@RequestBody PlProductDTO dto){
    	if(null == dto || StringUtils.isBlank(dto.getName())){
    		return ListResult.ok(Lists.newArrayList());
    	}
    	Long shopId = SessionContext.getShopId();
    	List<SimpleDataVO> list = plProductService.inklingQuery(dto.getName(), shopId);
    	
		return ListResult.ok(list);
    }
    
    @ApiOperation(value = "商品名缓存查询")
    @PostMapping("/cache-query")
    public ListResult<SimpleDataVO> cacheQuery(@RequestBody PlProductDTO dto){
		return ListResult.ok(Lists.newArrayList());
    }
    
}
