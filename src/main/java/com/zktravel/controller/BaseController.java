package com.zktravel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zktravel.enums.ICodeMessage;
import com.zktravel.util.ValidUtils;

public abstract class BaseController {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    protected final static int EXPORT_MAX_ROWS = 2000;
    protected final static int ASYN_EXPORT_MAX_ROWS = 50000;

    protected void notNull(Object obj) {
        ValidUtils.notNull(obj);
    }

    protected void notNull(Object obj, ICodeMessage codeMsg) {
        ValidUtils.notNull(obj, codeMsg);
    }

    protected void notEmpty(Object[] obj) {
        ValidUtils.notEmpty(obj);
    }

    protected void fieldNotNull(Object obj, String... fields) {
        ValidUtils.fieldNotNull(obj, fields);
    }
}
