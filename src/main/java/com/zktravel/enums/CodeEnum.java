package com.zktravel.enums;

public enum CodeEnum implements ICodeMessage {
    OK(0, "成功"),
    
    PRODUCT_STOCK_NUM_LT_0(1, "商品库存不足"),

    ID_NOT_NULL(-201, "id不能为空"),
    PARAMS_NOT_NULL(-203, "参数不能为空"),
    NODATA(-204, "code.nodata"),

    UNAUTHORIZED(-401, "code.unauthorized"),

    FORBIDDEN(-403, "code.forbidden"),


    SERVICE_ERROR(-599, "code.service.error"),

    VALIDATE_ERROR(-598, "code.validate.error"),

    NOT_NULL(-598, "code.notNull"),
    NOT_EMPTY(-598, "code.notEmpty"),

    PARAMETER_NOT_NULL(-598, "code.parameter.notNull"),
    PARAMETER_NOT_EMPTY(-598, "code.parameter.notEmpty"),


    EXISTS(-596, "code.validate.exists"),
    NOT_EXISTS(-595, "code.validate.notExists"),

    ERROR(-500, "系统繁忙,请稍后重试"),
    ERROR_UID(-501, "uid和sid必传"),
    ERROR_GET_USER_INFO(-502, "用户信息获取失败"),

    PRODUCT_TOP_TYPE_NOT_NULL(-601, "类型不能为空"),
    PRODUCT_TOP_BRAND_NOT_NULL(-602, "品牌id不能为空"),
    PRODUCT_ID_NOT_NULL(-603, "商品id不能为空"),
    PRODUCT_TOP_GT_MAX(-604, "置顶数量已满"),
    PRODUCT_TOP_ID_SORT_NOT_NULL(-605, "id和sort不能为空"),
    TA_PRODUCT_SELL_PRICE_NOT_NULL(-606, "售价不能为空或小于0"),
    TA_PRODUCT_PROFIT_AMT_LT_0(-607, "利润小于0，不能更改"),
    PRODUCT_STATUS_NOT_NULL(-608, "状态不能为空"),
    PRODUCT_NUM_GT_0(-609, "商品数量必须大于0"),
    PRODUCT_NOT_EXIST(-610, "商品不存在"),
    SELL_PRICE_LT_MIN_PRICE(-611, "商品价格不能小于最低价"),
    PRODUCT_LIST_NOT_NULL(-612, "商品列表不能为空"),
    NUM_NOT_NULL(-613, "显示数量不能为空"),
    SORT_NOT_NULL(-614, "排序不能为空"),
    CATEGORY_NOT_NULL(-615, "类目不能为空"),
    BRAND_PRODUCT_NOT_NULL(-616, "brandId和productId至少传一个"),
    PRODUCT_NOT_COUPON(-617, "该商品没有优惠"),
    COUPON_EXIST(-618, "优惠券已领取过"),
    CART_ID_NOT_NULL(-619, "购物车cartIds不能为空"),
    ADDRESS_NOT_NULL(-620, "收货地址必填"),
    USERNAME_NOT_NULL(-621, "联系人必填"),
    MOBILE_NOT_NULL(-622, "手机号必填"),
    IDCARD_ERROR(-623, "身份证号不正确"),
    REMARK_LENGTH_LONG(-624, "备注长度太长"),
    SHOP_NOT_EXIST(-625, "店铺不存在"),
    USER_NOT_EXIST(-626, "用户不存在"),
    TYPE_MISSING(-627, "无效的类型"),


    SHOP_TEMPLATE_NOT_NULL(-650, "模板不能为空");

    private Integer code;
    private String message;

    private CodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
