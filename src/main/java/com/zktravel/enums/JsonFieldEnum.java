package com.zktravel.enums;

public enum JsonFieldEnum {
	//字段类型
	invoiceType,
	invoiceTitle,
	invoiceContent,
	receiver,
	recMobile,
	receiverAddress,
	invoiceTelephone,
	invoiceTaxnum,
	bank,
	bankNum,
	deliveryProvinceCode,
	deliveryCityCode,
	deliveryCountyCode,
	deliveryAreaCode,
	deliveryAddress,
	deliveryName,
	deliveryMobile,
	deliveryPhone,
	deliveryPostcode,
	creatorName,
	creatorMobile,
	creatorPhone,
	creatorEmail,
	pmOriloc,
	pmOricon,
	sensitivity,
	model,
	savingEnergy
	;
}
