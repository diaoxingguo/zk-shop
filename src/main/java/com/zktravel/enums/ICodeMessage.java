package com.zktravel.enums;

public interface ICodeMessage {

	Integer getCode();
	
	String getMessage();
}
