package com.zktravel.constants;

import java.math.BigDecimal;

public class OrderConstant {
	//包邮最低价
	public static final BigDecimal EXEMPTION_POSTAGE_B = new BigDecimal(290);
	public static final BigDecimal EXEMPTION_POSTAGE_C = new BigDecimal(499);
	
	//邮费
	public static final BigDecimal POST_AMT = new BigDecimal(20);
}
