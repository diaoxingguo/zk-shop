package com.zktravel.constants;

import java.util.HashMap;
import java.util.Map;

public class ShopConstant {

    public static final int PAGE_FIRST = 1;

    // 类型 | 1 品牌 | 2 欧洲必买 | 3 新品上架 | 4 我的
    public static final int TYPE_BRAND = 1;
    public static final int TYPE_EUROPE_BUY = 2;
    public static final int TYPE_NEW_PRODUCT = 3;
    public static final int TYPE_MINE = 4;


    public static final Map<Integer, String> TYPEMAP = new HashMap<Integer, String>() {
        {
            put(TYPE_BRAND, "大牌推荐");
            put(TYPE_EUROPE_BUY, "欧洲必买");
            put(TYPE_NEW_PRODUCT, "新品上架");
            put(TYPE_MINE, "我的专栏");
        }
    };
}
