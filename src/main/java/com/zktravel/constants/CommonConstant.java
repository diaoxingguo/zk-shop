package com.zktravel.constants;

public class CommonConstant {

    public static final int AVAILABLE = 0;//可用

    public static final int DISAVAILABLE = 1;//不可用

    public static final Integer YES = 1;

    public static final Integer NO = 0;

    public static final Integer ON_SHELVES = 0;//上架
    public static final Integer TAKEN_OFF = 1;//下架

    public static final int TOP = 1;//置顶

    public static final int TOP_SORT = 1;

    public static final Integer PLATFORM_TYPE_B = 1; //B端
    public static final Integer PLATFORM_TYPE_C = 2; //C端

    public static final int DAY_SEVEN = 7;//过期时间


    public static final String CODE_PREFIX_SH = "SH";//code前缀
    
    public static final Integer CONCAT_LENGTH_30 = 30;//字符串拼接长度
}
