package com.zktravel.constants;

import java.math.BigDecimal;

public class ProductConstant {

    public static final int PAGENO_0 = 0;
    public static final int PAGENO_1 = 1;
    public static final int PAGESIZE_LIMIT_4 = 4;
    public static final int PAGESIZE_LIMIT_6 = 6;

    //商品排序: 1热销、2利润
    public static final Integer ORDERBY_SELL_NUM = 1;
    public static final Integer ORDERBY_PROFIR_AMT = 2;
    public static final Integer ORDERBY_NEW = 3;

    //最低价和最高价
    public static final BigDecimal PRICE_MIN = new BigDecimal(1);
    public static final BigDecimal PRICE_MAX_C = new BigDecimal(500);
    public static final BigDecimal PRICE_MAX_B = new BigDecimal(300);

    public static final Integer STOCK_AREA_IN = 1;//国内
    public static final Integer STOCK_AREA_OUT = 2;//国外
}
