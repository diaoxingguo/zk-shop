/**
 * 
 */
package com.zktravel.support;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author diaoxingguo
 *http://localhost:8080/zk-shop/swagger-ui.html#/
 */
/*@Configuration
@EnableWebMvc
@EnableSwagger2*/
public class SwaggerConfig {
	
    /*private final static String SWAGGER_TITLE = "API";

	@Bean
    public Docket createUserRestApi() {
		return doCreateRestApi("com.zktravel.controller", "接口", "接口文档");
    }
	
    private Docket doCreateRestApi(String basePackage, String group, String description) {
		Predicate<RequestHandler> predicate = RequestHandlerSelectors.basePackage(basePackage)
				.and(RequestHandlerSelectors.withClassAnnotation(Api.class))
				.and(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class));
		
		//添加head参数start  
        ParameterBuilder tokenPar = new ParameterBuilder();  
        List<Parameter> pars = new ArrayList<Parameter>();  
        tokenPar.name("x-shop-sid").description("店铺id").modelRef(new ModelRef("Long")).parameterType("header").required(true) .build();  
        ParameterBuilder tokenPar2 = new ParameterBuilder(); 
        tokenPar2.name("x-shop-uid").description("用户id").modelRef(new ModelRef("Long")).parameterType("header").required(true)
        .build();  
        
        pars.add(tokenPar.build());  
        pars.add(tokenPar2.build());  
		
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(description)).groupName(group)
                .select()
                .apis(t -> predicate.test(t))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(pars);
    }

	private ApiInfo apiInfo(String description) {
		return new ApiInfoBuilder().title(SWAGGER_TITLE)
				.description(description)
				.termsOfServiceUrl("/swagger-ui.html")
				.version("1.0")
				.build();
	}*/
}
