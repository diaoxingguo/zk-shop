package com.zktravel.support;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zktravel.core.exception.VisibleException;
import com.zktravel.core.vo.Result;
import com.zktravel.enums.CodeEnum;
import com.zktravel.util.CommonHelper;
import com.zktravel.util.ValidUtils;

@ControllerAdvice
public class GlobalExceptionHandler {
	private final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	public Object handleIllegalArgumentException(IllegalArgumentException e, HttpServletRequest req, HttpServletResponse response) {
		logger.debug("handleArgumentException---", e);
		logger.error(e.getMessage() + (e.getCause() != null && StringUtils.isNotBlank(e.getCause().getMessage())? 
        		" caused by: " + e.getCause().getMessage(): ""));
		
		return new Result(CodeEnum.VALIDATE_ERROR);
	}

	@ExceptionHandler(VisibleException.class)
	@ResponseBody
	public Object handleVisibleException(VisibleException e, HttpServletRequest req, HttpServletResponse response) {
		logger.debug("VisibleException---", e);
		logger.error(e.getMessage() + (e.getCause() != null && StringUtils.isNotBlank(e.getCause().getMessage())? 
        		" caused by: " + e.getCause().getMessage(): ""));
		return new Result(e.getCode(), e.getMessage());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseBody
	public Object handleValidException(MethodArgumentNotValidException e, HttpServletRequest req, HttpServletResponse response) {
		String validMsg = ValidUtils.extractErrorMessage(e);
		
		logger.debug("MethodArgumentNotValidException---", e);
		logger.info(validMsg);
		
		return new Result(CodeEnum.VALIDATE_ERROR.getCode(), validMsg);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseBody
	public Object handleValidException(ConstraintViolationException e, HttpServletRequest req, HttpServletResponse response) {
		String validMsg = ValidUtils.extractErrorMessage(e);

		logger.debug("ConstraintViolationException---", e);
		logger.info(validMsg);
		
		if (validMsg != null) return new Result(CodeEnum.VALIDATE_ERROR.getCode(), validMsg);
		
		return new Result(CodeEnum.VALIDATE_ERROR);
	}
	
	
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public Object handleException(Exception e, HttpServletRequest req, HttpServletResponse response) {
		logger.error("handleException---", e);
		
		String errMsg = CommonHelper.getMsgOnErr();
		if (errMsg != null) {
			return new Result(CodeEnum.ERROR.getCode(), errMsg);
		}
		
		return Result.ERROR;
	}
	
}
