package com.zktravel.mapper;

import com.zktravel.model.vo.user.ShopVO;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.po.Shop;

public interface ShopMapper extends Mapper<Shop> {

    void updateTemplate(Shop shop);

    ShopVO selectById(@Param("id") Long id);
}
