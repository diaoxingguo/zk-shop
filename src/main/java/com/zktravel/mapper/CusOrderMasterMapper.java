package com.zktravel.mapper;

import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.po.CusOrderMaster;

public interface CusOrderMasterMapper extends Mapper<CusOrderMaster>{

	void save(CusOrderMaster so);

}
