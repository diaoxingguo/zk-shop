package com.zktravel.mapper;

import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.po.CusOrderAddress;
import com.zktravel.model.vo.order.CusOrderAddressVO;

public interface CusOrderAddressMapper extends Mapper<CusOrderAddress>{

	CusOrderAddressVO getLatestInfo(@Param("userId") Long userId);

	void save(CusOrderAddress address);

}
