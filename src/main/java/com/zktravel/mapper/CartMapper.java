package com.zktravel.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.dto.cart.CartDTO;
import com.zktravel.model.po.Cart;
import com.zktravel.model.vo.cart.CartVO;

public interface CartMapper extends Mapper<Cart>{

	Integer getCartNum(@Param("userId") Long userId, @Param("shopId") Long shopId, @Param("platformType") Integer platformType);

	int deleteByIds(CartDTO dto);

	CartVO getCartProductInfo(CartDTO dto);

	void updateNum(CartDTO dto);

	List<CartVO> listShop(CartDTO dto);

	Integer listShopCount(CartDTO dto);

	void updateNumByProduct(Cart updateCart);

	List<CartVO> listProduct(@Param("list") List<Long> cartIds, @Param("userId") Long userId, 
			@Param("shopId") Long shopId, @Param("platformType") Integer platformType);

	List<CartVO> listParentProduct(@Param("list") List<Long> cartIds, @Param("userId") Long userId,
			@Param("shopId") Long shopId, @Param("platformType") Integer platformType);

	Cart getCartInfo(Cart cart);

}
