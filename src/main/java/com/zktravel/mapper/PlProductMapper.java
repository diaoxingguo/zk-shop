package com.zktravel.mapper;

import java.util.List;

import com.zktravel.model.vo.product.PdQueryParams;

import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.dto.product.PlProductDTO;
import com.zktravel.model.po.PlProduct;
import com.zktravel.model.vo.product.PlProductVO;

public interface PlProductMapper extends Mapper<PlProduct> {
    List<PlProductVO> list(PlProductDTO productDTO);

    List<PlProductVO> listBrandCate(PlProductDTO prodDTO);

    List<Long> listCategoryByBrandId(@Param("brandId") Long brand, @Param("shopId") Long shopId);

    PlProduct selectById(Long id);

    List<PlProductVO> togeListPage(PlProductDTO dto);

    /**
     * B端模块分类检索
     *
     * @param qry 检索条件
     */
    List<PlProductVO> getProductList(PdQueryParams qry);

    /**
     * B端模块分类检索 总数
     *
     * @param qry 检索条件
     */
    Long getProductCount(PdQueryParams qry);
    
	int updateStockAndSellNum(List<PlProduct> plProductList);

	String getLackStockName(List<PlProduct> plProductList);
}
