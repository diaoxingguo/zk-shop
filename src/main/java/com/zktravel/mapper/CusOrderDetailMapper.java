package com.zktravel.mapper;

import java.util.List;

import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.po.CusOrderDetail;

public interface CusOrderDetailMapper extends Mapper<CusOrderDetail>{

	void batchInsert(List<CusOrderDetail> itemList);

}
