package com.zktravel.mapper;

import java.util.List;

import com.zktravel.model.vo.coupon.CouListVO;
import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.po.CusCoupon;

public interface CusCouponMapper extends Mapper<CusCoupon> {

    CusCoupon exist(CusCoupon cusCoupon);

    List<CusCoupon> listByTpids(@Param("list") List<Long> tpidList, @Param("expireTime") Integer expireTime,
                                @Param("userId") Long userId, @Param("shopId") Long shopId);

    List<CouListVO> listByUserId(
            @Param("userId") Long userId,
            @Param("expireTime") Integer expireTime,
            @Param("expired") Integer expired,
            @Param("status") Integer status
    );

    int deleteByIds(@Param("list") List<Long> couponIdList);

}
