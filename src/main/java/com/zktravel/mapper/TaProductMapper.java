package com.zktravel.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.po.TaProduct;
import com.zktravel.model.vo.common.SimpleDataVO;
import com.zktravel.model.vo.product.PlProductVO;
import com.zktravel.model.vo.product.TaProductVO;

public interface TaProductMapper extends Mapper<TaProduct>{

	TaProductVO selectById(@Param("id") Long id, @Param("shopId") Long shopId);

	void update(TaProduct taProduct);

	List<Long> getCategoryIdListOff(@Param("shopId") Long shopId);

	PlProductVO selectProdByTpid(@Param("id") Long id, @Param("shopId") Long shopId);

	List<SimpleDataVO> inklingQuery(@Param("name") String name, @Param("shopId") Long shopId);

}
