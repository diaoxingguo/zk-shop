package com.zktravel.mapper;

import java.util.List;

import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.dto.template.TaProductTopDTO;
import com.zktravel.model.po.TaProductTop;
import com.zktravel.model.vo.product.TaProductTopVO;

public interface TaProductTopMapper extends Mapper<TaProductTop>{

	List<TaProductTopVO> getListByShopId(TaProductTopDTO vo);

	void updateSort(List<TaProductTopDTO> list);

	void cancelTop(TaProductTop top);

	void cancelTopBySort(TaProductTop top);

	Integer getTopNum(TaProductTop top);
}
