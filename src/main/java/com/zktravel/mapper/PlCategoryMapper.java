package com.zktravel.mapper;

import com.zktravel.model.vo.category.PlCategoryVO;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.po.PlCategory;

import java.util.List;

public interface PlCategoryMapper extends Mapper<PlCategory> {
    List<PlCategoryVO> getUnSelectList(@Param("list") List<Integer> idList);

    List<PlCategoryVO> listAll();
}
