package com.zktravel.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.po.PlBrand;
import com.zktravel.model.vo.brand.PlBrandVO;

public interface PlBrandMapper extends Mapper<PlBrand> {
    List<PlBrandVO> list(@Param("shopId") Long shopId, @Param("pageNo") int pageNo, @Param("limit") int limit);
}
