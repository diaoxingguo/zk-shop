package com.zktravel.mapper;

import tk.mybatis.mapper.common.Mapper;

import com.zktravel.model.po.TaCoupon;

public interface TaCouponMapper extends Mapper<TaCoupon>{

}
