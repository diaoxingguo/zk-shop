package com.zktravel.model.po;

import java.math.BigDecimal;

/**
 * 
 * @CreateDate 2018-05-08
 */
public class TaProduct extends BasePO {
	/**
	 * 商品id
	 */
	private Long productId;
	/**
	 * 0上架1下架
	 */
	private Integer status;
	/**
	 * 售价
	 */
	private BigDecimal sellPrice;
	
	private Long shopId;
	
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getStatus() {
		return status;
	}
	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}
	public BigDecimal getSellPrice() {
		return sellPrice;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
}