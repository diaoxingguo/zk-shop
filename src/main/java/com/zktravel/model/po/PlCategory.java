package com.zktravel.model.po;

import java.util.Date;

public class PlCategory {

    private Long id;

    /**
     * 父级分类ID
     */
    private Integer parentId;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 海报图片
     */
    private String Image;

    /**
     * 分类排序
     */
    private Integer sort;

    /**
     * 分类B端默认商品数量
     */
    private Integer TplNum;


    /**
     * 是否默认分组，1未默认，0不为默认
     */
    private Integer isDefault;

    /**
     *
     */
    private Date createdAt;

    /**
     *
     */
    private Date updatedAt;

    /**
     *
     */
    private Date deletedAt;

    /**
     * 状态 0 正常，9删除
     */
    private Integer status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSort() {
        return sort;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getTplNum() {
        return TplNum;
    }

    public void setTplNum(Integer tplNum) {
        TplNum = tplNum;
    }
}