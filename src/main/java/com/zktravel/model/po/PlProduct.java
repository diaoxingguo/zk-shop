package com.zktravel.model.po;

import java.math.BigDecimal;
import java.util.Date;

public class PlProduct extends BasePO {
    /**
     * 类目id
     */
    private Long categoryId;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品主图
     */
    private String mainImageUrl;
    /**
     * 采购价，平台成本价
     */
    private BigDecimal supplierPrice;
    /**
     * 品牌id
     */
    private Long brandId;
    /**
     * 品牌名称
     */
    private String brandName;
    /**
     * 柜面售价
     */
    private BigDecimal counterPrice;
    /**
     * 成本价，对导游的成本价
     */
    private BigDecimal marketPrice;
    /**
     * VIP成本价，对导游的成本价
     */
    private BigDecimal marketPriceVip;
    /**
     * 最低价
     */
    private BigDecimal minPrice;
    /**
     * 存库量
     */
    private Integer stockNum;
    /**
     * 销售数量
     */
    private Integer sellNum;
    /**
     * 虚拟销量
     */
    private Integer vSellNum;
    /**
     * 库存位置 0未设置1国内2国外
     */
    private Integer stockArea;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 0平台商品1商家（导游）商品
     */
    private Integer isPlatform;
    /**
     * 商家id
     */
    private Long shopId;
    /**
     * 供应商id
     */
    private Long spId;
    /**
     * 0是上架 1是下架
     */
    private Integer status;
    /**
     * 图片
     */
    private String images;
    /**
     * 商品简介，推荐语
     */
    private String briefIntroduction;
    /**
     * 详情
     */
    private String content;
    /**
     * 单位描述
     */
    private String unitDesc;
    /**
     * 创建时间
     */
    private Date createdAt;
    /**
     * 修改时间
     */
    private Date updatedAt;
    /**
     * 供应商名字
     */
    private String spName;

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setMainImageUrl(String mainImageUrl) {
        this.mainImageUrl = mainImageUrl;
    }

    public String getMainImageUrl() {
        return mainImageUrl;
    }

    public void setSupplierPrice(BigDecimal supplierPrice) {
        this.supplierPrice = supplierPrice;
    }

    public BigDecimal getSupplierPrice() {
        return supplierPrice;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setCounterPrice(BigDecimal counterPrice) {
        this.counterPrice = counterPrice;
    }

    public BigDecimal getCounterPrice() {
        return counterPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setSellNum(Integer sellNum) {
        this.sellNum = sellNum;
    }

    public Integer getSellNum() {
        return sellNum;
    }

    public void setStockArea(Integer stockArea) {
        this.stockArea = stockArea;
    }

    public Integer getStockArea() {
        return stockArea;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSort() {
        return sort;
    }

    public void setIsPlatform(Integer isPlatform) {
        this.isPlatform = isPlatform;
    }

    public Integer getIsPlatform() {
        return isPlatform;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setSpId(Long spId) {
        this.spId = spId;
    }

    public Long getSpId() {
        return spId;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getImages() {
        return images;
    }

    public void setBriefIntroduction(String briefIntroduction) {
        this.briefIntroduction = briefIntroduction;
    }

    public String getBriefIntroduction() {
        return briefIntroduction;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public String getUnitDesc() {
        return unitDesc;
    }

    public void setUnitDesc(String unitDesc) {
        this.unitDesc = unitDesc;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public BigDecimal getMarketPriceVip() {
        return marketPriceVip;
    }

    public void setMarketPriceVip(BigDecimal marketPriceVip) {
        this.marketPriceVip = marketPriceVip;
    }

    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getvSellNum() {
        return vSellNum;
    }

    public void setvSellNum(Integer vSellNum) {
        this.vSellNum = vSellNum;
    }
}