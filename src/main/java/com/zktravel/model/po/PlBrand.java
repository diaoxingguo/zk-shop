package com.zktravel.model.po;


public class PlBrand extends BasePO {
    /**
     * 品牌名称
     */
    private String name;
    /**
     * 品牌英文名称
     */
    private String nameEn;
    /**
     * 品牌别名
     */
    private String alias;
    /**
     * 品牌url地址
     */
    private String logoUrl;
    /**
     * 品牌介绍
     */
    private String introduction;


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getIntroduction() {
        return introduction;
    }
}