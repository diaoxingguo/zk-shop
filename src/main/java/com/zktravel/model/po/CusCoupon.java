package com.zktravel.model.po;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Id;

/**
 * 
 * @CreateDate 2018-05-08
 */
public class CusCoupon{
	
	@Id
	private Long id;
	/**
	 * 券金额
	 */
	private BigDecimal amt;
	/**
	 * 未修改之前的金额
	 */
	private BigDecimal lastAmt;
	/**
	 * 领取时的金额
	 */
	private BigDecimal orgAmt;
	/**
	 * 店铺id
	 */
	private Long shopId;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 商品id
	 */
	private Long tpid;
	/**
	 * 导游店铺活动券id，ta_coupon
	 */
	private Long tcid;
	
	/**
	 * 活动券状态，1有效，0无效
	 */
	private Integer status;
	/**
	 * 活动券类型，RD立减券
	 */
	private String type;
	
	/**
	 * 领取时间，时间戳
	 */
	private Integer receiveTime;
	/**
	 * 到期时间，时间戳
	 */
	private Integer expireTime;
	/**
	 * 是否需要提醒立减券金额已经变化，0不需要，1需要
	 */
	private Integer needNotice;
	/**
	 * 下一次过期提醒提前的天数
	 */
	private Integer expireNoticeDay;
	
	/**
	 * 
	 */
	private Date createdAt;
	/**
	 * 
	 */
	private Date updatedAt;
	/**
	 * 
	 */
	private Date deletedAt;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public BigDecimal getLastAmt() {
		return lastAmt;
	}
	public void setLastAmt(BigDecimal lastAmt) {
		this.lastAmt = lastAmt;
	}
	public BigDecimal getOrgAmt() {
		return orgAmt;
	}
	public void setOrgAmt(BigDecimal orgAmt) {
		this.orgAmt = orgAmt;
	}
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getTpid() {
		return tpid;
	}
	public void setTpid(Long tpid) {
		this.tpid = tpid;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getReceiveTime() {
		return receiveTime;
	}
	public void setReceiveTime(Integer receiveTime) {
		this.receiveTime = receiveTime;
	}
	public Integer getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(Integer expireTime) {
		this.expireTime = expireTime;
	}
	public Integer getNeedNotice() {
		return needNotice;
	}
	public void setNeedNotice(Integer needNotice) {
		this.needNotice = needNotice;
	}
	public Integer getExpireNoticeDay() {
		return expireNoticeDay;
	}
	public void setExpireNoticeDay(Integer expireNoticeDay) {
		this.expireNoticeDay = expireNoticeDay;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Date getDeletedAt() {
		return deletedAt;
	}
	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
	public Long getTcid() {
		return tcid;
	}
	public void setTcid(Long tcid) {
		this.tcid = tcid;
	}
	
}