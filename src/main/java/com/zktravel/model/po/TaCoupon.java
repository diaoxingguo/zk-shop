package com.zktravel.model.po;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @CreateDate 2018-05-08
 */
public class TaCoupon{
	
	private Long id;
	/**
	 * 券金额
	 */
	private BigDecimal amt;
	/**
	 * 店铺id
	 */
	private Integer shopId;
	/**
	 * 店铺货品id
	 */
	private Long taMchId;
	/**
	 * 活动券状态，0未开通、1开通、9失效
	 */
	private Integer status;
	/**
	 * 活动券类型，RD立减券
	 */
	private String type;
	/**
	 * 
	 */
	private Date createdAt;
	/**
	 * 
	 */
	private Date updatedAt;
	/**
	 * 
	 */
	private Date deletedAt;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setTaMchId(Long taMchId) {
		this.taMchId = taMchId;
	}
	public Long getTaMchId() {
		return taMchId;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getStatus() {
		return status;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
	public Date getDeletedAt() {
		return deletedAt;
	}
}