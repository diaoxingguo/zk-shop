package com.zktravel.model.po;


/**
 * 
 * @CreateDate 2018-05-08
 */
public class Cart extends BasePO{
	/**
	 * 店铺id
	 */
	private Long shopId;
	/**
	 * 购买者id
	 */
	private Long userId;
	/**
	 * ta_product.id
	 */
	private Long tpid;
	/**
	 * pl_product.id
	 */
	private Long ppid;
	/**
	 * 商品数量
	 */
	private Integer num;
	
	/**
     * 平台类型
     */
    private Integer platformType;
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public Long getTpid() {
		return tpid;
	}
	public void setTpid(Long tpid) {
		this.tpid = tpid;
	}
	public Long getPpid() {
		return ppid;
	}
	public void setPpid(Long ppid) {
		this.ppid = ppid;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Integer getPlatformType() {
		return platformType;
	}
	public void setPlatformType(Integer platformType) {
		this.platformType = platformType;
	}
	
}