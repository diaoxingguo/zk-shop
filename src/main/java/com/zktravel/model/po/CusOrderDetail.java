package com.zktravel.model.po;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @CreateDate 2018-05-08
 */
public class CusOrderDetail {
	
	private Long id;
	
	/**
	 * 订单id
	 */
	private Long pid;
	/**
	 * 店铺ID
	 */
	private Long shopId;
	/**
	 * 购买者用户id
	 */
	private Long customId;
	/**
	 * 商品标题作为此标题的值
	 */
	private String mchName;
	
	private Long ppid;
	/**
	 * 商品主图片缩略图地址
	 */
	private String mchImg;
	/**
	 * SKU的值，即：商品的规格 例如：颜色:黑色;尺码:XL;材料:毛绒XL
	 */
	private String mchDesc;
	/**
	 * 利润
	 */
	private BigDecimal profitAmt;
	/**
	 * 总价
	 */
	private BigDecimal totalAmt;
	/**
	 * 单价价格
	 */
	private BigDecimal unitAmt;
	/**
	 * 下单时成本价
	 */
	private BigDecimal marketPrice;
	/**
	 * 下单时采购价
	 */
	private BigDecimal supplierPrice;
	/**
	 * 税费
	 */
	private BigDecimal taxAmt;
	/**
	 * 超级合伙人的店铺id
	 */
	private Long pShopId;
	/**
	 * 超级合伙人 分成利润
	 */
	private BigDecimal pProfitAmt;
	/**
	 * 优惠券id
	 */
	private Long couponId;
	/**
	 * 优惠券减免金额
	 */
	private BigDecimal couponAmt;
	/**
	 * 购买数量
	 */
	private Integer num;
	/**
	 * 支付状态：WAIT 待支付 CANCEL 取消 PAID 完成支付 SEND 发货 COMPLETED  签收或者完成 REFUND 退款
	 */
	private String status;
	/**
	 * 取消人：BUYER 买家取消 SELLER 卖家取消 AUTO 自动取消
	 */
	private String cancel;
	/**
	 * 
	 */
	private Date createdAt;
	/**
	 * 
	 */
	private Date updatedAt;
	/**
	 * 
	 */
	private Date deletedAt;
	/**
	 * 确认人id
	 */
	private Integer checkManagerId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setPid(Long pid) {
		this.pid = pid;
	}
	public Long getPid() {
		return pid;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setCustomId(Long customId) {
		this.customId = customId;
	}
	public Long getCustomId() {
		return customId;
	}
	public void setMchName(String mchName) {
		this.mchName = mchName;
	}
	public String getMchName() {
		return mchName;
	}
	public void setMchImg(String mchImg) {
		this.mchImg = mchImg;
	}
	public String getMchImg() {
		return mchImg;
	}
	public void setMchDesc(String mchDesc) {
		this.mchDesc = mchDesc;
	}
	public String getMchDesc() {
		return mchDesc;
	}
	public void setProfitAmt(BigDecimal profitAmt) {
		this.profitAmt = profitAmt;
	}
	public BigDecimal getProfitAmt() {
		return profitAmt;
	}
	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	public BigDecimal getTotalAmt() {
		return totalAmt;
	}
	public void setUnitAmt(BigDecimal unitAmt) {
		this.unitAmt = unitAmt;
	}
	public BigDecimal getUnitAmt() {
		return unitAmt;
	}
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	public void setSupplierPrice(BigDecimal supplierPrice) {
		this.supplierPrice = supplierPrice;
	}
	public BigDecimal getSupplierPrice() {
		return supplierPrice;
	}
	public void setTaxAmt(BigDecimal taxAmt) {
		this.taxAmt = taxAmt;
	}
	public BigDecimal getTaxAmt() {
		return taxAmt;
	}
	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}
	public Long getCouponId() {
		return couponId;
	}
	public void setCouponAmt(BigDecimal couponAmt) {
		this.couponAmt = couponAmt;
	}
	public BigDecimal getCouponAmt() {
		return couponAmt;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Integer getNum() {
		return num;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getCancel() {
		return cancel;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
	public Date getDeletedAt() {
		return deletedAt;
	}
	public void setCheckManagerId(Integer checkManagerId) {
		this.checkManagerId = checkManagerId;
	}
	public Integer getCheckManagerId() {
		return checkManagerId;
	}

	public Long getPpid() {
		return ppid;
	}

	public void setPpid(Long ppid) {
		this.ppid = ppid;
	}

	public Long getpShopId() {
		return pShopId;
	}

	public void setpShopId(Long pShopId) {
		this.pShopId = pShopId;
	}

	public BigDecimal getpProfitAmt() {
		return pProfitAmt;
	}

	public void setpProfitAmt(BigDecimal pProfitAmt) {
		this.pProfitAmt = pProfitAmt;
	}
}