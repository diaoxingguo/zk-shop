package com.zktravel.model.dto.brand;

import com.zktravel.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("品牌参数")
public class PlBrandDTO extends BaseDTO {
    /**
     * 品牌名称
     */
    @ApiModelProperty("品牌名称")
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}