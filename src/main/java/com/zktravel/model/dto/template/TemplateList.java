package com.zktravel.model.dto.template;

import com.zktravel.model.dto.BaseDTO;
import com.zktravel.model.dto.template.Template;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("店铺参数")
public class TemplateList extends BaseDTO {

    @ApiModelProperty("模板列表")
    private List<Template> template;
    private Template modify;

    public List<Template> getTemplate() {
        return template;
    }

    public Template getModify() {
        return modify;
    }

    public void setModify(Template modify) {
        this.modify = modify;
    }

    public void setTemplate(List<Template> template) {
        this.template = template;
    }


}