package com.zktravel.model.dto.template;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 *
 */
@ApiModel("模板")
public class Template {

    @ApiModelProperty("类型")
    private Integer type;//类型

    @ApiModelProperty("显示数量")
    private Integer num;//显示数量

    @ApiModelProperty("排序")
    private Integer sort;//排序

    @ApiModelProperty("名称")
    private String name;//名称

    @ApiModelProperty("海报图")
    private String image;//名称

    @ApiModelProperty("商品列表")
    private List<Object> list;

    public Template() {
    }

    public Template(Integer type, Integer num) {
        this.type = type;
        this.num = num;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }

    public List<Object> getList() {
        return list;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
