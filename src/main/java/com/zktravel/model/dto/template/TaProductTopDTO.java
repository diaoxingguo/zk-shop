package com.zktravel.model.dto.template;

import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("DTO")
public class TaProductTopDTO extends BaseDTO {
    /**
     * 商品id
     */
    @ApiModelProperty("商品id")
    private Long itemId;
    /**
     * 商铺id
     */
    @ApiModelProperty("商铺id")
    private Long shopId;
    /**
     * 类目id
     */
    @ApiModelProperty("类目id")
    private Long categoryId;

    /**
     * 排序数量越小优先级越高
     */
    @ApiModelProperty("排序数量越小优先级越高")
    private Integer sort;

    @ApiModelProperty("排序类型")
    private Integer orderby;

    private List<Long> categoryIdList;


    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() {
        return categoryId;
    }


    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSort() {
        return sort;
    }


    public Integer getOrderby() {
        return orderby;
    }

    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }

    public List<Long> getCategoryIdList() {
        return categoryIdList;
    }

    public void setCategoryIdList(List<Long> categoryIdList) {
        this.categoryIdList = categoryIdList;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public CodeEnum validTop() {
//        if (null == type) {
//            return CodeEnum.PRODUCT_TOP_TYPE_NOT_NULL;
//        }
//
//        if (type == ShopConstant.TYPE_BRAND && null == brandId) {
//            return CodeEnum.PRODUCT_TOP_BRAND_NOT_NULL;
//        }
//
//        if (type != ShopConstant.TYPE_BRAND && null == productId) {
//            return CodeEnum.PRODUCT_ID_NOT_NULL;
//        }

        return null;
    }
}