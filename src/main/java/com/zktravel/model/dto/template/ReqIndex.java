package com.zktravel.model.dto.template;

public class ReqIndex {
    private Long sid;

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }
}
