package com.zktravel.model.dto.template;

import java.util.List;

/**
 * 修改置顶顺序的参数
 */
public class TopSort {
    private Long categoryId;
    private List<Long> ids;


    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }
}
