package com.zktravel.model.dto.template;

import com.zktravel.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;

import java.util.List;


@ApiModel("DTO")
public class TaProductTopExtDTO extends BaseDTO {

    private List<TaProductTopDTO> list;

    public List<TaProductTopDTO> getList() {
        return list;
    }

    public void setList(List<TaProductTopDTO> list) {
        this.list = list;
    }
}