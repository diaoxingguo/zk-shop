package com.zktravel.model.dto;

import io.swagger.annotations.ApiModelProperty;

import com.zktravel.enums.CodeEnum;

public class BaseDTO {

    private Long id;

    private final static int DEFAULT_LIMIT = 20;// TODO
    private final static int DEFAULT_PAGE = 1;
    @ApiModelProperty("页码")
    private int pageNo = DEFAULT_PAGE;
    @ApiModelProperty("页大小")
    private int pageSize = DEFAULT_LIMIT;
    private Integer start;

    private Boolean isVip;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStart() {
        return (getPageNo() - 1) * this.getPageSize();
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public int getPageNo() {
        return pageNo > 0 ? pageNo : DEFAULT_PAGE;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize > 0 ? pageSize : DEFAULT_LIMIT;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public Boolean getIsVip() {
        return isVip;
    }

    public void setIsVip(Boolean isVip) {
        this.isVip = isVip;
    }

    public CodeEnum validateId() {
        if (null == id) {
            return CodeEnum.ID_NOT_NULL;
        }
        return null;
    }
}
