package com.zktravel.model.dto.cart;

import io.swagger.annotations.ApiModel;

@ApiModel("购物车C端")
public class ClientCartDTO {

	private Long ta_prod_id;
	
	/**
	 * 商品数量
	 */
	private Integer num;

	public Long getTa_prod_id() {
		return ta_prod_id;
	}

	public void setTa_prod_id(Long ta_prod_id) {
		this.ta_prod_id = ta_prod_id;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}
}
