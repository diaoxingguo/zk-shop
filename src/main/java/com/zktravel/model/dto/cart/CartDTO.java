package com.zktravel.model.dto.cart;

import java.util.List;

import com.zktravel.enums.CodeEnum;

import com.zktravel.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;

@ApiModel("购物车")
public class CartDTO extends BaseDTO {

    /**
     * 商品id
     */
    private Long productId;
    /**
     * 商品数量
     */
    private Integer num;

    private List<Long> ids;
    private Long uid;
    private Long sid;
    
    private Integer platformType;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }


    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public Integer getPlatformType() {
		return platformType;
	}

	public void setPlatformType(Integer platformType) {
		this.platformType = platformType;
	}

	public CodeEnum validate() {
        if (null == productId) {
            return CodeEnum.PRODUCT_ID_NOT_NULL;
        }
        if (null == num || num <= 0) {
            return CodeEnum.PRODUCT_NUM_GT_0;
        }
        return null;
    }
}
