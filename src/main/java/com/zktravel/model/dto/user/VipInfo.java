package com.zktravel.model.dto.user;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class VipInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Boolean flag;
	private Integer level;
	private Date vipTime;
	private Date lastBookingTime;
	private BigDecimal dealAmt;
	private BigDecimal totalAmt;
	private String remark;
	
	public Boolean getFlag() {
		return flag;
	}
	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public Date getVipTime() {
		return vipTime;
	}
	public void setVipTime(Date vipTime) {
		this.vipTime = vipTime;
	}
	public Date getLastBookingTime() {
		return lastBookingTime;
	}
	public void setLastBookingTime(Date lastBookingTime) {
		this.lastBookingTime = lastBookingTime;
	}
	public BigDecimal getDealAmt() {
		return dealAmt;
	}
	public void setDealAmt(BigDecimal dealAmt) {
		this.dealAmt = dealAmt;
	}
	public BigDecimal getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
