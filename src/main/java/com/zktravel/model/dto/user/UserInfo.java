package com.zktravel.model.dto.user;

import com.zktravel.model.dto.order.Card;

import java.io.Serializable;

public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long shopId;
    private String avatarUrl;
    private Long bind_tailor_id;

    private String cardUrl;
    private String nickName;
    private String qrUrl;
    private String levelName;
    private String identityPass;
    private VipInfo vipInfo;
    private Card card;
    private String wxOpenId;
    private ShopInfo shopInfo;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Long getBind_tailor_id() {
        return bind_tailor_id;
    }

    public void setBind_tailor_id(Long bind_tailor_id) {
        this.bind_tailor_id = bind_tailor_id;
    }

    public String getCardUrl() {
        return cardUrl;
    }

    public void setCardUrl(String cardUrl) {
        this.cardUrl = cardUrl;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getQrUrl() {
        return qrUrl;
    }

    public void setQrUrl(String qrUrl) {
        this.qrUrl = qrUrl;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public String getIdentityPass() {
        return identityPass;
    }

    public void setIdentityPass(String identityPass) {
        this.identityPass = identityPass;
    }

    public void setVipInfo(VipInfo info) {
        this.vipInfo = info;
    }

    public VipInfo getVipInfo() {
        return this.vipInfo;
    }

    public Boolean isVip() {
        return this.getVipInfo().getFlag();
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId;
    }

	public ShopInfo getShopInfo() {
		return shopInfo;
	}

	public void setShopInfo(ShopInfo shopInfo) {
		this.shopInfo = shopInfo;
	}
    
}
