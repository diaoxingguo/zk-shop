package com.zktravel.model.dto.category;

import com.zktravel.model.dto.brand.BrandItem;

import java.util.List;

public class CategoryGroup {

    private Long categoryId;

    private String categoryName;

    private List<BrandItem> brandList;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<BrandItem> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<BrandItem> brandList) {
        this.brandList = brandList;
    }
}
