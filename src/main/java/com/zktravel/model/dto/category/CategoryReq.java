package com.zktravel.model.dto.category;

import com.zktravel.model.dto.BaseDTO;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * 类目请求相关接口
 */
public class CategoryReq extends BaseDTO {
    /**
     * 类型
     */
    @ApiModelProperty("类型--类型0品牌1欧洲必买2新品上架")
    private Integer type;

    @ApiModelProperty("类目id")
    private Long categoryId;

    @ApiModelProperty("类型列表")
    private List<Integer> types;

    @ApiModelProperty("类目列表")
    private List<Long> categoryIds;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public List<Integer> getTypes() {
        return types;
    }

    public void setTypes(List<Integer> types) {
        this.types = types;
    }

    public List<Long> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<Long> categoryIds) {
        this.categoryIds = categoryIds;
    }
}
