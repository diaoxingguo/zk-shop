package com.zktravel.model.dto.coupon;

import com.zktravel.model.dto.BaseDTO;

public class CouListRequest extends BaseDTO {
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
