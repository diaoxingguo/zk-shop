package com.zktravel.model.dto.coupon;

import com.zktravel.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;

@ApiModel("用户优惠券")
public class CusCouponDTO extends BaseDTO {

    private Long sid;
    private Long goods_id;

    public Long getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(Long goods_id) {
        this.goods_id = goods_id;
    }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }
}
