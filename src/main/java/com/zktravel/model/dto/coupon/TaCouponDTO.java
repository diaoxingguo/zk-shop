package com.zktravel.model.dto.coupon;

import com.zktravel.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;

@ApiModel("导游优惠券")
public class TaCouponDTO extends BaseDTO {

    private Long goods_id;

    public Long getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(Long goods_id) {
        this.goods_id = goods_id;
    }

}
