package com.zktravel.model.dto.product;

import com.zktravel.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import com.zktravel.enums.CodeEnum;

/**
 * 
 * @CreateDate 2018-05-08
 */
 @ApiModel("商家商品")
public class TaProductDTO extends BaseDTO {
	
	 /**
	 * 商品id
	 */
	 @ApiModelProperty("商品id")
	 private Long productId;
	 /**
	 * 售价
	 */
	@ApiModelProperty("售价")
	private BigDecimal sellPrice;
	
	@ApiModelProperty("状态：0上架1下架")
	private Integer status;
	
	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}
	public BigDecimal getSellPrice() {
		return sellPrice;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public CodeEnum validateProductId() {
		if (null == productId) {
			return CodeEnum.PRODUCT_ID_NOT_NULL;
		}
		return null;
	}
}