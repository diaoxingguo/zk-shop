package com.zktravel.model.dto.product;

import java.math.BigDecimal;

/**
 * 商品Model
 */
public class ProductItem2B {
    private Long id;

    private String name;

    private String mainImageUrl;

    /**
     * 库存
     */
    private Integer stockNum;

    /**
     * 库存所在地
     * 0 未定义
     * 1 国内
     * 2 国外
     */
    private Integer stockArea;

    /**
     * 销量
     */
    private Integer sellNum;

    /**
     * 1 置顶 0 不置顶
     */
    private Integer top = 0;

    /**
     * 是否有VIP价格
     */
    private Boolean hasVip = false;

    /**
     * 默认柜面价
     * 系统默认C端售价
     */
    private BigDecimal counterPrice;

    /**
     * B端自定义价格
     */
    private BigDecimal sellPrice;

    /**
     * 导游结算价格
     */
    private BigDecimal marketPrice;

    /**
     * VIP导游结算价格
     */
    private BigDecimal marketPriceVip;

    /**
     * 普通利润
     */
    private BigDecimal profitAmt;

    /**
     * VIP利润
     */
    private BigDecimal profitAmtVip;


    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    public BigDecimal getProfitAmt() {
        return profitAmt;
    }

    public void setProfitAmt(BigDecimal profitAmt) {
        this.profitAmt = profitAmt;
    }

    public Long getId() {
        return id;
    }

    public Integer getStockArea() {
        return stockArea;
    }

    public void setStockArea(Integer stockArea) {
        this.stockArea = stockArea;
    }

    public Integer getSellNum() {
        return sellNum;
    }

    public void setSellNum(Integer sellNum) {
        this.sellNum = sellNum;
    }


    public BigDecimal getProfitAmtVip() {
        return profitAmtVip;
    }

    public void setProfitAmtVip(BigDecimal profitAmtVip) {
        this.profitAmtVip = profitAmtVip;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMainImageUrl() {
        return mainImageUrl;
    }

    public void setMainImageUrl(String mainImageUrl) {
        this.mainImageUrl = mainImageUrl;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getMarketPriceVip() {
        return marketPriceVip;
    }

    public void setMarketPriceVip(BigDecimal marketPriceVip) {
        this.marketPriceVip = marketPriceVip;
    }

    public BigDecimal getCounterPrice() {
        return counterPrice;
    }

    public void setCounterPrice(BigDecimal counterPrice) {
        this.counterPrice = counterPrice;
    }

    public Boolean getHasVip() {
        return hasVip;
    }

    public void setHasVip(Boolean hasVip) {
        this.hasVip = hasVip;
    }
}
