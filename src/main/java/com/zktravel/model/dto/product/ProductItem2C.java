package com.zktravel.model.dto.product;

import java.math.BigDecimal;

/**
 * 商品Model
 */
public class ProductItem2C {
    private Long id;

    private String name;

    private String mainImageUrl;

    /**
     * 库存
     */
    private Integer stockNum;

    /**
     * 库存所在地
     * 0 未定义
     * 1 国内
     * 2 国外
     */
    private Integer stockArea;

    /**
     * 销量
     */
    private Integer sellNum;

    /**
     * 1 置顶 0 不置顶
     */
    private Integer top = 0;

    /**
     * 默认柜面价
     * 系统默认C端售价
     */
    private BigDecimal counterPrice;

    /**
     * B端自定义价格
     */
    private BigDecimal sellPrice;

    private String briefIntroduction;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMainImageUrl() {
        return mainImageUrl;
    }

    public void setMainImageUrl(String mainImageUrl) {
        this.mainImageUrl = mainImageUrl;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getStockArea() {
        return stockArea;
    }

    public void setStockArea(Integer stockArea) {
        this.stockArea = stockArea;
    }

    public Integer getSellNum() {
        return sellNum;
    }

    public void setSellNum(Integer sellNum) {
        this.sellNum = sellNum;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public BigDecimal getCounterPrice() {
        return counterPrice;
    }

    public void setCounterPrice(BigDecimal counterPrice) {
        this.counterPrice = counterPrice;
    }

    public String getBriefIntroduction() {
        return briefIntroduction;
    }

    public void setBriefIntroduction(String briefIntroduction) {
        this.briefIntroduction = briefIntroduction;
    }
}

