package com.zktravel.model.dto.product;

import com.zktravel.constants.ShopConstant;
import com.zktravel.model.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * @CreateDate 2018-05-08
 */
@ApiModel("商品")
public class PlProductDTO extends BaseDTO {

    /**
     * 店铺id
     */
    private Long sid;
    /**
     * 店铺id
     */
    private Long shopId;
    /**
     * 类目id
     */
    @ApiModelProperty("类目id")
    private Long categoryId;
    /**
     * 商品名称
     */
    @ApiModelProperty("商品名称")
    private String name;

    @ApiModelProperty("品牌id")
    private Long brandId;


    private Integer status;
    /**
     * 品牌名称
     */
    @ApiModelProperty("品牌名称")
    private String brandName;
    /**
     * 排序
     */
    private Integer sort;

    //类型--默认欧洲必买
    @ApiModelProperty("类型--类型0品牌1欧洲必买2新品上架")
    private Integer type = ShopConstant.TYPE_EUROPE_BUY;

    //排序方式--热销、利润、新品（新品可不传值）
    @ApiModelProperty("排序方式--商品排序:1热销、2利润（新品不传值）")
    private Integer orderBy;

    private List<Long> idList;

    private List<Long> notIdList;

    private List<Long> categoryIdList;

    @ApiModelProperty("最低价")
    private BigDecimal priceMin;

    @ApiModelProperty("最高价")
    private BigDecimal priceMax;
    
    private Integer platformType;

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSort() {
        return sort;
    }

    public List<Long> getIdList() {
        return idList;
    }

    public void setIdList(List<Long> idList) {
        this.idList = idList;
    }

    public List<Long> getNotIdList() {
        return notIdList;
    }

    public void setNotIdList(List<Long> notIdList) {
        this.notIdList = notIdList;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public List<Long> getCategoryIdList() {
        return categoryIdList;
    }

    public void setCategoryIdList(List<Long> categoryIdList) {
        this.categoryIdList = categoryIdList;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(BigDecimal priceMin) {
        this.priceMin = priceMin;
    }

    public BigDecimal getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(BigDecimal priceMax) {
        this.priceMax = priceMax;
    }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

	public Integer getPlatformType() {
		return platformType;
	}

	public void setPlatformType(Integer platformType) {
		this.platformType = platformType;
	}

}