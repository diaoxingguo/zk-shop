package com.zktravel.model.dto.order;

import com.zktravel.enums.CodeEnum;
import com.zktravel.model.dto.BaseDTO;
import com.zktravel.model.vo.order.CusOrderAddressVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@ApiModel("订单")
public class CusOrderMasterDTO extends BaseDTO {

    private Long sid;


    @ApiModelProperty("购物车ids")
    private List<Long> cartIds;

    @ApiModelProperty("地址信息")
    private CusOrderAddressVO address;

    @ApiModelProperty("备注")
    private String remark;
    
    private String formId;
    
    private String formIdCreate;

    public List<Long> getCartIds() {
        return cartIds;
    }

    public void setCartIds(List<Long> cartIds) {
        this.cartIds = cartIds;
    }

    public CusOrderAddressVO getAddress() {
        return address;
    }

    public void setAddress(CusOrderAddressVO address) {
        this.address = address;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getFormIdCreate() {
		return formIdCreate;
	}

	public void setFormIdCreate(String formIdCreate) {
		this.formIdCreate = formIdCreate;
	}

	public CodeEnum validSubmit() {
        if (CollectionUtils.isEmpty(cartIds)) {
            return CodeEnum.CART_ID_NOT_NULL;
        }
        if (null == address) {
            return CodeEnum.ADDRESS_NOT_NULL;
        }

        if (StringUtils.isEmpty(address.getProvinceName()) || StringUtils.isEmpty(address.getCityName())
                || StringUtils.isEmpty(address.getCountyName()) || StringUtils.isEmpty(address.getDetailInfo())) {
            return CodeEnum.ADDRESS_NOT_NULL;
        }

        if (StringUtils.isEmpty(address.getUserName())) {
            return CodeEnum.USERNAME_NOT_NULL;
        }

        if (StringUtils.isEmpty(address.getTelNumber())) {
            return CodeEnum.MOBILE_NOT_NULL;
        }

        if (StringUtils.isNotBlank(remark) && remark.length() > 45) {
            return CodeEnum.REMARK_LENGTH_LONG;
        }
        return null;
    }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }
}
