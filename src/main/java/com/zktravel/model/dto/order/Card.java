package com.zktravel.model.dto.order;

import java.io.Serializable;

public class Card implements Serializable{

	private static final long serialVersionUID = 1L;
	private String weix;
	private String company;
	private String name;
	private String brief;
	private String email;
	private String phone;
	private String avatar;
	private String guide_area;
	private City city;
	
	public String getWeix() {
		return weix;
	}
	public void setWeix(String weix) {
		this.weix = weix;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBrief() {
		return brief;
	}
	public void setBrief(String brief) {
		this.brief = brief;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getGuide_area() {
		return guide_area;
	}
	public void setGuide_area(String guide_area) {
		this.guide_area = guide_area;
	}
	public City getCity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}
	
}
