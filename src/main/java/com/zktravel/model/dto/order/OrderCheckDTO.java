package com.zktravel.model.dto.order;

import io.swagger.annotations.ApiModel;

@ApiModel("订单检查接口")
public class OrderCheckDTO {

    private Long orderId;
    private Integer userId;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
