package com.zktravel.model.vo.category;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel("VO")
public class PlCategoryVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 分类名称
     */
    @ApiModelProperty("分类名称")
    private String name;

    /**
     * 海报图片
     */
    private String Image;

    /**
     * 分类排序
     */
    @ApiModelProperty("分类排序")
    private Integer sort;

    /**
     * 分类B端默认商品数量
     */
    private Integer TplNum;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSort() {
        return sort;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public Integer getTplNum() {
        return TplNum;
    }

    public void setTplNum(Integer tplNum) {
        TplNum = tplNum;
    }
}