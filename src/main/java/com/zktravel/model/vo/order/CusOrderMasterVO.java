package com.zktravel.model.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @CreateDate 2018-05-08
 */
 @ApiModel("VO")
public class CusOrderMasterVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	/**
	 * 
	 */
	@ApiModelProperty("")
	private Long shopId;
	/**
	 * 地址id
	 */
	@ApiModelProperty("地址id")
	private Long addressId;
	/**
	 * 订单编码
	 */
	@ApiModelProperty("订单编码")
	private String orderId;
	/**
	 * 购买者用户id
	 */
	@ApiModelProperty("购买者用户id")
	private Long customId;
	/**
	 * 商品数量
	 */
	@ApiModelProperty("商品数量")
	private Integer num;
	/**
	 * 预支付订单ID
	 */
	@ApiModelProperty("预支付订单ID")
	private String formId;
	/**
	 * 利润
	 */
	@ApiModelProperty("利润")
	private BigDecimal profitAmt;
	/**
	 * 总价
	 */
	@ApiModelProperty("总价")
	private BigDecimal mchAmt;
	/**
	 * 邮费
	 */
	@ApiModelProperty("邮费")
	private BigDecimal postAmt;
	/**
	 * 实付金额
	 */
	@ApiModelProperty("实付金额")
	private BigDecimal payAmt;
	/**
	 * 优惠券减免金额
	 */
	@ApiModelProperty("优惠券减免金额")
	private BigDecimal couponAmt;
	/**
	 * 支付方式：WX_PAY 微信支付 ALI_PAY 支付宝支付 UNION_PAY 银联支付 OTHER 其他支付
	 */
	@ApiModelProperty("支付方式：WX_PAY 微信支付 ALI_PAY 支付宝支付 UNION_PAY 银联支付 OTHER 其他支付")
	private String payType;
	/**
	 * 支付状态：WAIT 待支付 CANCEL 取消 PAID 完成支付 SEND 发货 COMPLETED  签收或者完成
	 */
	@ApiModelProperty("支付状态：WAIT 待支付 CANCEL 取消 PAID 完成支付 SEND 发货 COMPLETED  签收或者完成")
	private String status;
	/**
	 * 取消人：BUYER 买家取消 SELLER 卖家取消 AUTO 自动取消
	 */
	@ApiModelProperty("取消人：BUYER 买家取消 SELLER 卖家取消 AUTO 自动取消")
	private String cancel;
	/**
	 * 取消人：BUYER 买家确认 SELLER 卖家确认 AUTO 自动确认
	 */
	@ApiModelProperty("取消人：BUYER 买家确认 SELLER 卖家确认 AUTO 自动确认")
	private String complete;
	/**
	 * 创建时间时间戳
	 */
	@ApiModelProperty("创建时间时间戳")
	private Integer recordTime;
	/**
	 * 支付时间
	 */
	@ApiModelProperty("支付时间")
	private Integer paidAt;
	/**
	 * 签收时间或者订单完成时间
	 */
	@ApiModelProperty("签收时间或者订单完成时间")
	private Integer completedAt;
	/**
	 * 发货时间
	 */
	@ApiModelProperty("发货时间")
	private Integer sendAt;
	@ApiModelProperty("")
	private Date deletedAt;
	/**
	 * 创建订单的formid
	 */
	@ApiModelProperty("创建订单的formid")
	private String formIdCreate;
	/**
	 * 支付的formid
	 */
	@ApiModelProperty("支付的formid")
	private String formIdPay;
	/**
	 * 关闭订单提醒0未通知1已经通知
	 */
	@ApiModelProperty("关闭订单提醒0未通知1已经通知")
	private Integer closeNotice;
	/**
	 * remark
	 */
	@ApiModelProperty("remark")
	private String remark;
	/**
	 * 支付回调信息
	 */
	@ApiModelProperty("支付回调信息")
	private String paymentInfo;
	/**
	 * 退款回调信息
	 */
	@ApiModelProperty("退款回调信息")
	private String refundInfo;
	/**
	 * 税费
	 */
	@ApiModelProperty("税费")
	private BigDecimal taxAmt;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}
	public Long getAddressId() {
		return addressId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setCustomId(Long customId) {
		this.customId = customId;
	}
	public Long getCustomId() {
		return customId;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Integer getNum() {
		return num;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	public String getFormId() {
		return formId;
	}
	public void setProfitAmt(BigDecimal profitAmt) {
		this.profitAmt = profitAmt;
	}
	public BigDecimal getProfitAmt() {
		return profitAmt;
	}
	public void setMchAmt(BigDecimal mchAmt) {
		this.mchAmt = mchAmt;
	}
	public BigDecimal getMchAmt() {
		return mchAmt;
	}
	public void setPostAmt(BigDecimal postAmt) {
		this.postAmt = postAmt;
	}
	public BigDecimal getPostAmt() {
		return postAmt;
	}
	public void setPayAmt(BigDecimal payAmt) {
		this.payAmt = payAmt;
	}
	public BigDecimal getPayAmt() {
		return payAmt;
	}
	public void setCouponAmt(BigDecimal couponAmt) {
		this.couponAmt = couponAmt;
	}
	public BigDecimal getCouponAmt() {
		return couponAmt;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getPayType() {
		return payType;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getCancel() {
		return cancel;
	}
	public void setComplete(String complete) {
		this.complete = complete;
	}
	public String getComplete() {
		return complete;
	}
	public void setRecordTime(Integer recordTime) {
		this.recordTime = recordTime;
	}
	public Integer getRecordTime() {
		return recordTime;
	}
	public void setPaidAt(Integer paidAt) {
		this.paidAt = paidAt;
	}
	public Integer getPaidAt() {
		return paidAt;
	}
	public void setCompletedAt(Integer completedAt) {
		this.completedAt = completedAt;
	}
	public Integer getCompletedAt() {
		return completedAt;
	}
	public void setSendAt(Integer sendAt) {
		this.sendAt = sendAt;
	}
	public Integer getSendAt() {
		return sendAt;
	}
	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
	public Date getDeletedAt() {
		return deletedAt;
	}
	public void setFormIdCreate(String formIdCreate) {
		this.formIdCreate = formIdCreate;
	}
	public String getFormIdCreate() {
		return formIdCreate;
	}
	public void setFormIdPay(String formIdPay) {
		this.formIdPay = formIdPay;
	}
	public String getFormIdPay() {
		return formIdPay;
	}
	public void setCloseNotice(Integer closeNotice) {
		this.closeNotice = closeNotice;
	}
	public Integer getCloseNotice() {
		return closeNotice;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRemark() {
		return remark;
	}
	public void setPaymentInfo(String paymentInfo) {
		this.paymentInfo = paymentInfo;
	}
	public String getPaymentInfo() {
		return paymentInfo;
	}
	public void setRefundInfo(String refundInfo) {
		this.refundInfo = refundInfo;
	}
	public String getRefundInfo() {
		return refundInfo;
	}
	public void setTaxAmt(BigDecimal taxAmt) {
		this.taxAmt = taxAmt;
	}
	public BigDecimal getTaxAmt() {
		return taxAmt;
	}
}