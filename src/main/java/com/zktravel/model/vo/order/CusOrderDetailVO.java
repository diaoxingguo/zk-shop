package com.zktravel.model.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @CreateDate 2018-05-08
 */
 @ApiModel("VO")
public class CusOrderDetailVO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long id;
	 
	/**
	 * 订单id
	 */
	@ApiModelProperty("订单id")
	private Long pid;
	/**
	 * 店铺ID
	 */
	@ApiModelProperty("店铺ID")
	private Long shopId;
	/**
	 * 购买者用户id
	 */
	@ApiModelProperty("购买者用户id")
	private Long customId;
	/**
	 * 商品标题作为此标题的值
	 */
	@ApiModelProperty("商品标题作为此标题的值")
	private String mchName;
	/**
	 * 商品IDphp
	 */
	@ApiModelProperty("商品IDphp")
	private Long mchId;
	/**
	 * 规格产品ID
	 */
	@ApiModelProperty("规格产品ID")
	private Integer productId;
	/**
	 * 商品主图片缩略图地址
	 */
	@ApiModelProperty("商品主图片缩略图地址")
	private String mchImg;
	/**
	 * SKU的值，即：商品的规格 例如：颜色:黑色;尺码:XL;材料:毛绒XL
	 */
	@ApiModelProperty("SKU的值，即：商品的规格 例如：颜色:黑色;尺码:XL;材料:毛绒XL")
	private String mchDesc;
	/**
	 * 利润
	 */
	@ApiModelProperty("利润")
	private BigDecimal profitAmt;
	/**
	 * 总价
	 */
	@ApiModelProperty("总价")
	private BigDecimal totalAmt;
	/**
	 * 单价价格
	 */
	@ApiModelProperty("单价价格")
	private BigDecimal unitAmt;
	/**
	 * 下单时成本价
	 */
	@ApiModelProperty("下单时成本价")
	private BigDecimal marketPrice;
	/**
	 * 下单时采购价
	 */
	@ApiModelProperty("下单时采购价")
	private BigDecimal supplierPrice;
	/**
	 * 税费
	 */
	@ApiModelProperty("税费")
	private BigDecimal taxAmt;
	/**
	 * 优惠券id
	 */
	@ApiModelProperty("优惠券id")
	private Integer couponId;
	/**
	 * 优惠券减免金额
	 */
	@ApiModelProperty("优惠券减免金额")
	private BigDecimal couponAmt;
	/**
	 * 购买数量
	 */
	@ApiModelProperty("购买数量")
	private Integer num;
	/**
	 * 支付状态：WAIT 待支付 CANCEL 取消 PAID 完成支付 SEND 发货 COMPLETED  签收或者完成 REFUND 退款
	 */
	@ApiModelProperty("支付状态：WAIT 待支付 CANCEL 取消 PAID 完成支付 SEND 发货 COMPLETED  签收或者完成 REFUND 退款")
	private String status;
	/**
	 * 取消人：BUYER 买家取消 SELLER 卖家取消 AUTO 自动取消
	 */
	@ApiModelProperty("取消人：BUYER 买家取消 SELLER 卖家取消 AUTO 自动取消")
	private String cancel;

	@ApiModelProperty("")
	private Date deletedAt;
	/**
	 * 确认人id
	 */
	@ApiModelProperty("确认人id")
	private Integer checkManagerId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setPid(Long pid) {
		this.pid = pid;
	}
	public Long getPid() {
		return pid;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setCustomId(Long customId) {
		this.customId = customId;
	}
	public Long getCustomId() {
		return customId;
	}
	public void setMchName(String mchName) {
		this.mchName = mchName;
	}
	public String getMchName() {
		return mchName;
	}
	public void setMchId(Long mchId) {
		this.mchId = mchId;
	}
	public Long getMchId() {
		return mchId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setMchImg(String mchImg) {
		this.mchImg = mchImg;
	}
	public String getMchImg() {
		return mchImg;
	}
	public void setMchDesc(String mchDesc) {
		this.mchDesc = mchDesc;
	}
	public String getMchDesc() {
		return mchDesc;
	}
	public void setProfitAmt(BigDecimal profitAmt) {
		this.profitAmt = profitAmt;
	}
	public BigDecimal getProfitAmt() {
		return profitAmt;
	}
	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	public BigDecimal getTotalAmt() {
		return totalAmt;
	}
	public void setUnitAmt(BigDecimal unitAmt) {
		this.unitAmt = unitAmt;
	}
	public BigDecimal getUnitAmt() {
		return unitAmt;
	}
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	public void setSupplierPrice(BigDecimal supplierPrice) {
		this.supplierPrice = supplierPrice;
	}
	public BigDecimal getSupplierPrice() {
		return supplierPrice;
	}
	public void setTaxAmt(BigDecimal taxAmt) {
		this.taxAmt = taxAmt;
	}
	public BigDecimal getTaxAmt() {
		return taxAmt;
	}
	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}
	public Integer getCouponId() {
		return couponId;
	}
	public void setCouponAmt(BigDecimal couponAmt) {
		this.couponAmt = couponAmt;
	}
	public BigDecimal getCouponAmt() {
		return couponAmt;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Integer getNum() {
		return num;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}
	public String getCancel() {
		return cancel;
	}
	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
	public Date getDeletedAt() {
		return deletedAt;
	}
	public void setCheckManagerId(Integer checkManagerId) {
		this.checkManagerId = checkManagerId;
	}
	public Integer getCheckManagerId() {
		return checkManagerId;
	}
}