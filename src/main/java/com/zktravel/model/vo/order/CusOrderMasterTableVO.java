package com.zktravel.model.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @CreateDate 2018-05-08
 */
 @ApiModel("VO")
public class CusOrderMasterTableVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	/**
	 * 
	 */
	@ApiModelProperty("")
	private Long shop_id;
	/**
	 * 地址id
	 */
	@ApiModelProperty("地址id")
	private Long address_id;
	/**
	 * 订单编码
	 */
	@ApiModelProperty("订单编码")
	private String order_id;
	/**
	 * 购买者用户id
	 */
	@ApiModelProperty("购买者用户id")
	private Long custom_id;
	/**
	 * 商品数量
	 */
	@ApiModelProperty("商品数量")
	private Integer num;
	/**
	 * 预支付订单ID
	 */
	@ApiModelProperty("预支付订单ID")
	private String form_id;
	/**
	 * 利润
	 */
	@ApiModelProperty("利润")
	private BigDecimal profit_amt;
	/**
	 * 总价
	 */
	@ApiModelProperty("总价")
	private BigDecimal mch_amt;
	/**
	 * 邮费
	 */
	@ApiModelProperty("邮费")
	private BigDecimal post_amt;
	/**
	 * 实付金额
	 */
	@ApiModelProperty("实付金额")
	private BigDecimal pay_amt;
	/**
	 * 优惠券减免金额
	 */
	@ApiModelProperty("优惠券减免金额")
	private BigDecimal coupon_amt;
	/**
	 * 支付方式：WX_PAY 微信支付 ALI_PAY 支付宝支付 UNION_PAY 银联支付 OTHER 其他支付
	 */
	@ApiModelProperty("支付方式：WX_PAY 微信支付 ALI_PAY 支付宝支付 UNION_PAY 银联支付 OTHER 其他支付")
	private String pay_type;
	/**
	 * 支付状态：WAIT 待支付 CANCEL 取消 PAID 完成支付 SEND 发货 COMPLETED  签收或者完成
	 */
	@ApiModelProperty("支付状态：WAIT 待支付 CANCEL 取消 PAID 完成支付 SEND 发货 COMPLETED  签收或者完成")
	private String status;
	/**
	 * 创建时间时间戳
	 */
	@ApiModelProperty("创建时间时间戳")
	private Integer record_time;
	/**
	 * 创建订单的formid
	 */
	@ApiModelProperty("创建订单的formid")
	private String form_id_create;
	/**
	 * remark
	 */
	@ApiModelProperty("remark")
	private String remark;
	/**
	 * 税费
	 */
	@ApiModelProperty("税费")
	private BigDecimal tax_amt;
	
	private Date created_at;
	
	private Date updated_at;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShop_id() {
		return shop_id;
	}
	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}
	public Long getAddress_id() {
		return address_id;
	}
	public void setAddress_id(Long address_id) {
		this.address_id = address_id;
	}
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public Long getCustom_id() {
		return custom_id;
	}
	public void setCustom_id(Long custom_id) {
		this.custom_id = custom_id;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public BigDecimal getProfit_amt() {
		return profit_amt;
	}
	public void setProfit_amt(BigDecimal profit_amt) {
		this.profit_amt = profit_amt;
	}
	public BigDecimal getMch_amt() {
		return mch_amt;
	}
	public void setMch_amt(BigDecimal mch_amt) {
		this.mch_amt = mch_amt;
	}
	public BigDecimal getPost_amt() {
		return post_amt;
	}
	public void setPost_amt(BigDecimal post_amt) {
		this.post_amt = post_amt;
	}
	public BigDecimal getPay_amt() {
		return pay_amt;
	}
	public void setPay_amt(BigDecimal pay_amt) {
		this.pay_amt = pay_amt;
	}
	public BigDecimal getCoupon_amt() {
		return coupon_amt;
	}
	public void setCoupon_amt(BigDecimal coupon_amt) {
		this.coupon_amt = coupon_amt;
	}
	public String getPay_type() {
		return pay_type;
	}
	public void setPay_type(String pay_type) {
		this.pay_type = pay_type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getRecord_time() {
		return record_time;
	}
	public void setRecord_time(Integer record_time) {
		this.record_time = record_time;
	}
	public String getForm_id_create() {
		return form_id_create;
	}
	public void setForm_id_create(String form_id_create) {
		this.form_id_create = form_id_create;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public BigDecimal getTax_amt() {
		return tax_amt;
	}
	public void setTax_amt(BigDecimal tax_amt) {
		this.tax_amt = tax_amt;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}
	
}