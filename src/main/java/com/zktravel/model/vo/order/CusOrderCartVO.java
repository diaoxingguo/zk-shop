package com.zktravel.model.vo.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @CreateDate 2018-05-08
 */
 @ApiModel("VO")
public class CusOrderCartVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	/**
	 * 店铺id
	 */
	@ApiModelProperty("店铺id")
	private Long shopId;
	/**
	 * 购买者id
	 */
	@ApiModelProperty("购买者id")
	private Long customId;
	/**
	 * 店铺商品id
	 */
	@ApiModelProperty("店铺商品id")
	private Long taMchId;
	/**
	 * 
	 */
	@ApiModelProperty("")
	private Long taProdId;
	/**
	 * 商品名称
	 */
	@ApiModelProperty("商品名称")
	private String mchName;
	/**
	 * 商品图片
	 */
	@ApiModelProperty("商品图片")
	private String mchImg;
	/**
	 * SKU的值，即：商品的规格 例如：颜色:黑色;尺码:XL;材料:毛绒XL
	 */
	@ApiModelProperty("SKU的值，即：商品的规格 例如：颜色:黑色;尺码:XL;材料:毛绒XL")
	private String mchDesc;
	/**
	 * 
	 */
	@ApiModelProperty("")
	private Date deletedAt;
	/**
	 * 总价
	 */
	@ApiModelProperty("总价")
	private BigDecimal totalAmt;
	/**
	 * 单价
	 */
	@ApiModelProperty("单价")
	private BigDecimal unitAmt;
	/**
	 * 商品数量
	 */
	@ApiModelProperty("商品数量")
	private Integer num;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setCustomId(Long customId) {
		this.customId = customId;
	}
	public Long getCustomId() {
		return customId;
	}
	public void setTaMchId(Long taMchId) {
		this.taMchId = taMchId;
	}
	public Long getTaMchId() {
		return taMchId;
	}
	public void setTaProdId(Long taProdId) {
		this.taProdId = taProdId;
	}
	public Long getTaProdId() {
		return taProdId;
	}
	public void setMchName(String mchName) {
		this.mchName = mchName;
	}
	public String getMchName() {
		return mchName;
	}
	public void setMchImg(String mchImg) {
		this.mchImg = mchImg;
	}
	public String getMchImg() {
		return mchImg;
	}
	public void setMchDesc(String mchDesc) {
		this.mchDesc = mchDesc;
	}
	public String getMchDesc() {
		return mchDesc;
	}
	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
	public Date getDeletedAt() {
		return deletedAt;
	}
	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	public BigDecimal getTotalAmt() {
		return totalAmt;
	}
	public void setUnitAmt(BigDecimal unitAmt) {
		this.unitAmt = unitAmt;
	}
	public BigDecimal getUnitAmt() {
		return unitAmt;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Integer getNum() {
		return num;
	}
}