package com.zktravel.model.vo.cart;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import com.zktravel.model.vo.BaseVO;


/**
 * 
 * @CreateDate 2018-05-08
 */
public class CartVO extends BaseVO {
	/**
	 * 店铺id
	 */
	private Long shopId;
	/**
	 * 购买者id
	 */
	private Long userId;
	/**
	 * 商品id
	 */
	@ApiModelProperty("商品id")
	private Long ppid;
	
	private Long productId;
	/**
	 * 购物车商品数量
	 */
	@ApiModelProperty("购物车商品数量")
	private Integer num;
	/**
	 * 商品名称
	 */
	@ApiModelProperty("商品名称")
	private String name;
	/**
	 * 单位描述
	 */
	@ApiModelProperty("单位描述")
	private String unitDesc;
	/**
	 * 商品主图
	 */
	@ApiModelProperty("商品主图")
	private String mainImageUrl;
	/**
	 * 存库量
	 */
	@ApiModelProperty("存库量")
	private Integer stockNum;
	
	@ApiModelProperty("库存位置 0未设置1国内2国外")
	private Integer stockArea;
	
	@ApiModelProperty("成本价，对导游的成本价")
	private BigDecimal marketPrice;
	
	@ApiModelProperty("VIP成本价，对导游的成本价")
	private BigDecimal marketPriceVip;
	
	@ApiModelProperty("对导游的成本总价")
	private BigDecimal totalMarketPrice;
	
	@ApiModelProperty("对VIP导游的成本总价")
	private BigDecimal totalMarketPriceVip;
	
	@ApiModelProperty("销售价")
	private BigDecimal sellPrice;
	
	private Long couponId;
	
	private BigDecimal couponAmt;
	
	@ApiModelProperty("柜面价")
	private BigDecimal counterPrice;
	
	private transient BigDecimal supplierPrice;
	
	private transient BigDecimal marPrice;

	private transient BigDecimal profitAmt;
	
	@ApiModelProperty("到期时间")
	private Long expireTime;
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMainImageUrl() {
		return mainImageUrl;
	}
	public void setMainImageUrl(String mainImageUrl) {
		this.mainImageUrl = mainImageUrl;
	}
	public Integer getStockNum() {
		return stockNum;
	}
	public void setStockNum(Integer stockNum) {
		this.stockNum = stockNum;
	}
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	public BigDecimal getMarketPriceVip() {
		return marketPriceVip;
	}
	public void setMarketPriceVip(BigDecimal marketPriceVip) {
		this.marketPriceVip = marketPriceVip;
	}
	public BigDecimal getTotalMarketPrice() {
		if(null == num || num.compareTo(0) <= 0){
			num = 0;
		}
		if(null == marketPrice || marketPrice.compareTo(BigDecimal.ZERO) <= 0){
			marketPrice = BigDecimal.ZERO;
		}
		totalMarketPrice = marketPrice.multiply(new BigDecimal(num)).setScale(2, BigDecimal.ROUND_HALF_UP);
		
		return totalMarketPrice;
	}
	public void setTotalMarketPrice(BigDecimal totalMarketPrice) {
		this.totalMarketPrice = totalMarketPrice;
	}
	public BigDecimal getTotalMarketPriceVip() {
		if(null == num || num.compareTo(0) <= 0){
			num = 0;
		}
		if(null == marketPriceVip || marketPriceVip.compareTo(BigDecimal.ZERO) <= 0){
			marketPriceVip = BigDecimal.ZERO;
		}
		totalMarketPriceVip = marketPriceVip.multiply(new BigDecimal(num)).setScale(2, BigDecimal.ROUND_HALF_UP);
		
		return totalMarketPriceVip;
	}
	public void setTotalMarketPriceVip(BigDecimal totalMarketPriceVip) {
		this.totalMarketPriceVip = totalMarketPriceVip;
	}
	public Integer getStockArea() {
		return stockArea;
	}
	public void setStockArea(Integer stockArea) {
		this.stockArea = stockArea;
	}
	public String getUnitDesc() {
		return unitDesc;
	}
	public void setUnitDesc(String unitDesc) {
		this.unitDesc = unitDesc;
	}
	public BigDecimal getSellPrice() {
		return sellPrice;
	}
	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}
	public Long getCouponId() {
		return couponId;
	}
	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}
	public BigDecimal getCouponAmt() {
		return couponAmt;
	}
	public void setCouponAmt(BigDecimal couponAmt) {
		this.couponAmt = couponAmt;
	}
	public Long getPpid() {
		return ppid;
	}
	public void setPpid(Long ppid) {
		this.ppid = ppid;
	}
	public BigDecimal getSupplierPrice() {
		return supplierPrice;
	}
	public void setSupplierPrice(BigDecimal supplierPrice) {
		this.supplierPrice = supplierPrice;
	}
	public BigDecimal getMarPrice() {
		return marPrice;
	}
	public void setMarPrice(BigDecimal marPrice) {
		this.marPrice = marPrice;
	}
	public BigDecimal getProfitAmt() {
		return profitAmt;
	}
	public void setProfitAmt(BigDecimal profitAmt) {
		this.profitAmt = profitAmt;
	}

	public Long getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Long expireTime) {
		this.expireTime = expireTime;
	}

	public BigDecimal getCounterPrice() {
		return counterPrice;
	}
	public void setCounterPrice(BigDecimal counterPrice) {
		this.counterPrice = counterPrice;
	}
	
}