package com.zktravel.model.vo.cart;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;


/**
 * 
 * @CreateDate 2018-05-08
 */
public class CartExtVO{
	
	@ApiModelProperty("购物车列表")
	private List<CartVO> cartList;
	
	@ApiModelProperty("总价")
	private BigDecimal totalAmt;
	
	@ApiModelProperty("节省总价")
	private BigDecimal leaveAmt;
	
	@ApiModelProperty("邮费")
	private BigDecimal postAmt;
	
	@ApiModelProperty("税费")
	private BigDecimal taxAmt;
	
	
	public List<CartVO> getCartList() {
		return cartList;
	}

	public void setCartList(List<CartVO> cartList) {
		this.cartList = cartList;
	}

	public BigDecimal getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}

	public BigDecimal getLeaveAmt() {
		return leaveAmt;
	}

	public void setLeaveAmt(BigDecimal leaveAmt) {
		this.leaveAmt = leaveAmt;
	}

	public BigDecimal getPostAmt() {
		return postAmt;
	}

	public void setPostAmt(BigDecimal postAmt) {
		this.postAmt = postAmt;
	}

	public BigDecimal getTaxAmt() {
		return taxAmt;
	}

	public void setTaxAmt(BigDecimal taxAmt) {
		this.taxAmt = taxAmt;
	}
	
}