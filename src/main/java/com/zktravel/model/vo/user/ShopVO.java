package com.zktravel.model.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @CreateDate 2018-05-08
 */
@ApiModel("VO")
public class ShopVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long parentId;
    /**
     *
     */
    @ApiModelProperty("")
    private String name;
    /**
     * 店铺创建人
     */
    @ApiModelProperty("店铺创建人")
    private Long userId;
    private Long parentUserId;
    /**
     * 店铺总额，支付加、退款减
     */
    @ApiModelProperty("店铺总额，支付加、退款减")
    private BigDecimal amount;
    /**
     * 店铺余额，可提现利润，发货加、提现减
     */
    @ApiModelProperty("店铺余额，可提现利润，发货加、提现减")
    private BigDecimal accountAmt;
    /**
     * 店铺总利润，发货后加
     */
    @ApiModelProperty("店铺总利润，发货后加")
    private BigDecimal profitAmt;
    /**
     * 店铺状态，1已发布，0未发布，9已删除
     */
    @ApiModelProperty("店铺状态，1已发布，0未发布，9已删除")
    private Integer status;
    /**
     * 店铺主页模板，json格式数组
     */
    @ApiModelProperty("店铺主页模板，json格式数组")
    private String template;
    /**
     *
     */
    @ApiModelProperty("")
    private Date createdAt;
    /**
     *
     */
    @ApiModelProperty("")
    private Date updatedAt;
    /**
     *
     */
    @ApiModelProperty("")
    private Date deletedAt;

    /**
     *
     */
    @ApiModelProperty("")
    private Integer newOrder;
    /**
     * 店主的微信openid-发消息用
     */
    @ApiModelProperty("店主的微信openid-发消息用")
    private String wxOpenId;
    /**
     * 是否认证，0未认证，1已认证
     */
    @ApiModelProperty("是否认证，0未认证，1已认证")
    private Integer isCertificated;
    /**
     * 认证类型，IC身份证，PT护照
     */
    @ApiModelProperty("认证类型，IC身份证，PT护照")
    private String ciType;
    /**
     * 认证姓名
     */
    @ApiModelProperty("认证姓名")
    private String ciName;
    /**
     * 证件号
     */
    @ApiModelProperty("证件号")
    private String ciNo;
    /**
     * 联系电话
     */
    @ApiModelProperty("联系电话")
    private String ciPhone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAccountAmt(BigDecimal accountAmt) {
        this.accountAmt = accountAmt;
    }

    public BigDecimal getAccountAmt() {
        return accountAmt;
    }

    public void setProfitAmount(BigDecimal profitAmount) {
        this.profitAmt = profitAmount;
    }

    public BigDecimal getProfitAmount() {
        return profitAmt;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setNewOrder(Integer newOrder) {
        this.newOrder = newOrder;
    }

    public Integer getNewOrder() {
        return newOrder;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId;
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setIsCertificated(Integer isCertificated) {
        this.isCertificated = isCertificated;
    }

    public Integer getIsCertificated() {
        return isCertificated;
    }

    public void setCiType(String ciType) {
        this.ciType = ciType;
    }

    public String getCiType() {
        return ciType;
    }

    public void setCiName(String ciName) {
        this.ciName = ciName;
    }

    public String getCiName() {
        return ciName;
    }

    public void setCiNo(String ciNo) {
        this.ciNo = ciNo;
    }

    public String getCiNo() {
        return ciNo;
    }

    public void setCiPhone(String ciPhone) {
        this.ciPhone = ciPhone;
    }

    public String getCiPhone() {
        return ciPhone;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getParentUserId() {
        return parentUserId;
    }

    public void setParentUserId(Long parentUserId) {
        this.parentUserId = parentUserId;
    }

    public BigDecimal getProfitAmt() {
        return profitAmt;
    }

    public void setProfitAmt(BigDecimal profitAmt) {
        this.profitAmt = profitAmt;
    }
}