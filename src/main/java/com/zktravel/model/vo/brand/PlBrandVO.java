package com.zktravel.model.vo.brand;

import com.zktravel.model.vo.BaseVO;
import io.swagger.annotations.ApiModelProperty;


public class PlBrandVO extends BaseVO {
    /**
     * 品牌名称
     */
    @ApiModelProperty("品牌名称")
    private String name;
    /**
     * 品牌英文名称
     */
    @ApiModelProperty("品牌英文名称")
    private String nameEn;
    /**
     * 品牌别名
     */
    @ApiModelProperty("品牌别名")
    private String alias;
    /**
     * 品牌url地址
     */
    @ApiModelProperty("品牌url地址")
    private String logoUrl;
    /**
     * 品牌介绍
     */
    @ApiModelProperty("品牌介绍")
    private String introduction;
    /**
     * 是否可用:默认0，0可用，1不可用
     */
    @ApiModelProperty("是否可用:默认0，0可用，1不可用")
    private Integer status;

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 排序
     */
    private Integer sort;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }
}