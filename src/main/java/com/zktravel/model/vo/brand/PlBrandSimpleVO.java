package com.zktravel.model.vo.brand;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @CreateDate 2018-05-08
 */
 @ApiModel("品牌")
public class PlBrandSimpleVO{
	 
	private Long id;
	 /**
	 * 品牌名称
	 */
	@ApiModelProperty("品牌名称")
	private String name;
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}