package com.zktravel.model.vo.product;

/**
 * 暂时用于B端 18-06-15
 * 商品检索条件
 */
public class PdQueryParams {

    /**
     * 商品分类
     */
    private Long categoryId;


    private Long shopId;

    /**
     * 排序规则
     */
    private Integer orderBy;

    /**
     * 品牌
     */
    private Long brandId;

    /**
     * 置顶条件
     */
    private Integer topType;

    private Integer pageNo;

    private Integer pageSize;

    private Integer startNo;

    public Integer getStartNo() {
        return (this.pageNo - 1) * pageSize;
    }

    private Boolean isVip;

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long catId) {
        this.categoryId = catId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Integer getTopType() {
        return topType;
    }

    public void setTopType(Integer topType) {
        this.topType = topType;
    }

    public Boolean getVip() {
        return isVip;
    }

    public void setVip(Boolean vip) {
        isVip = vip;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public void setStartNo(Integer startNo) {
        this.startNo = startNo;
    }
}
