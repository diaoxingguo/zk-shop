package com.zktravel.model.vo.product;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.zktravel.model.vo.BaseVO;

@ApiModel("VO")
public class PlProductVO extends BaseVO {
    /**
     * 类目id
     */
    @ApiModelProperty("类目id")
    private Long categoryId;

    /**
     * 商品名称
     */
    @ApiModelProperty("商品名称")
    private String name;

    /**
     * 商品主图
     */
    @ApiModelProperty("商品主图")
    private String mainImageUrl;

    /**
     * 品牌id
     */
    @ApiModelProperty("品牌id")
    private Long brandId;

    /**
     * 品牌名称
     */
    @ApiModelProperty("品牌名称")
    private String brandName;

    /**
     * 柜面售价
     */
    @ApiModelProperty("柜面售价")
    private BigDecimal counterPrice;

    /**
     * 成本价，对导游的成本价
     */
    @ApiModelProperty("成本价，对导游的成本价")
    private BigDecimal marketPrice;

    /**
     * 成本价，对导游的成本价
     */
    @ApiModelProperty("VIP成本价，对导游的成本价")
    private BigDecimal marketPriceVip;

    @ApiModelProperty("最低价")
    private BigDecimal minPrice;

    /**
     * 存库量
     */
    @ApiModelProperty("存库量")
    private Integer stockNum;

    /**
     * 销售数量
     */
    @ApiModelProperty("销售数量")
    private Integer sellNum;

    /**
     * 库存位置 0未设置1国内2国外
     */
    @ApiModelProperty("库存位置 0未设置1国内2国外")
    private Integer stockArea;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer sort;

    /**
     * 0平台商品1商家（导游）商品
     */
    @ApiModelProperty("0平台商品1商家（导游）商品")
    private Integer isPlatform;

    /**
     * 商家id
     */
    @ApiModelProperty("商家id")
    private Long shopId;

    /**
     * 供应商id
     */
    @ApiModelProperty("供应商id")
    private Long spId;

    /**
     * 0是上架 1是下架
     */
    @ApiModelProperty("0是上架 1是下架")
    private Integer status;

    /**
     * 图片
     */
    @ApiModelProperty("图片")
    private String images;

    @ApiModelProperty("图片数组")
    private String[] imageArray;

    /**
     * 商品简介，推荐语
     */
    @ApiModelProperty("商品简介，推荐语")
    private String briefIntroduction;

    /**
     * 详情
     */
    @ApiModelProperty("详情")
    private String content;

    /**
     * 规格json数组
     */
    @ApiModelProperty("规格json数组")
    private String unitDesc;

    /**
     * 售价
     */
    @ApiModelProperty("售价")
    private BigDecimal sellPrice;

    @ApiModelProperty("利润")
    private BigDecimal profitAmt;

    @ApiModelProperty("利润Vip")
    private BigDecimal profitAmtVip;

    @ApiModelProperty("供应商名字")
    private String spName;

    @ApiModelProperty("优惠金额")
    private BigDecimal couponAmt;

    @ApiModelProperty("用户已领券")
    private Boolean userHasCoupon;
    @ApiModelProperty("用户已领券金额")
    private BigDecimal userCouponAmt;
    @ApiModelProperty("优惠券过期时间")
    private Integer couponExpireTime;
    private String couponExpireDate;

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setMainImageUrl(String mainImageUrl) {
        this.mainImageUrl = mainImageUrl;
    }

    public String getMainImageUrl() {
        return mainImageUrl;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setCounterPrice(BigDecimal counterPrice) {
        this.counterPrice = counterPrice;
    }

    public BigDecimal getCounterPrice() {
        return counterPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setSellNum(Integer sellNum) {
        this.sellNum = sellNum;
    }

    public Integer getSellNum() {
        return sellNum;
    }

    public void setStockArea(Integer stockArea) {
        this.stockArea = stockArea;
    }

    public Integer getStockArea() {
        return stockArea;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSort() {
        return sort;
    }

    public void setIsPlatform(Integer isPlatform) {
        this.isPlatform = isPlatform;
    }

    public Integer getIsPlatform() {
        return isPlatform;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setSpId(Long spId) {
        this.spId = spId;
    }

    public Long getSpId() {
        return spId;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getImages() {
        return images;
    }

    public void setBriefIntroduction(String briefIntroduction) {
        this.briefIntroduction = briefIntroduction;
    }

    public String getBriefIntroduction() {
        return briefIntroduction;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public BigDecimal getMarketPriceVip() {
        return marketPriceVip;
    }

    public void setMarketPriceVip(BigDecimal marketPriceVip) {
        this.marketPriceVip = marketPriceVip;
    }

    public String getUnitDesc() {
        return unitDesc;
    }

    public void setUnitDesc(String unitDesc) {
        this.unitDesc = unitDesc;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public BigDecimal getProfitAmt() {

        if (null != sellPrice && null != marketPrice) {
            profitAmt = sellPrice.subtract(marketPrice);
        }
        return profitAmt;
    }

    public void setProfitAmt(BigDecimal profitAmt) {
        this.profitAmt = profitAmt;
    }

    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName;
    }

    public BigDecimal getCouponAmt() {
        if (null != counterPrice && null != sellPrice && counterPrice.compareTo(sellPrice) > 0) {
            couponAmt = counterPrice.subtract(sellPrice).setScale(2, BigDecimal.ROUND_HALF_UP);
        }
        return couponAmt;
    }

    public void setCouponAmt(BigDecimal couponAmt) {
        this.couponAmt = couponAmt;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getProfitAmtVip() {
        if (null != sellPrice && null != marketPriceVip) {
            profitAmt = sellPrice.subtract(marketPriceVip);
        }
        return profitAmtVip;
    }

    public void setProfitAmtVip(BigDecimal profitAmtVip) {
        this.profitAmtVip = profitAmtVip;
    }

    public String[] getImageArray() {
        if (StringUtils.isNotBlank(images) && images.length() > 3) {
            String image1 = images.substring(1, images.length());
            String image2 = image1.substring(0, image1.length() - 1);
            imageArray = StringEscapeUtils.unescapeJava(image2).replace("\"", "").split(",");
        }
        return imageArray;
    }

    public void setImageArray(String[] imageArray) {
        this.imageArray = imageArray;
    }

    public Boolean getUserHasCoupon() {
        return userHasCoupon;
    }

    public void setUserHasCoupon(Boolean userHasCoupon) {
        this.userHasCoupon = userHasCoupon;
    }

    public BigDecimal getUserCouponAmt() {
        return userCouponAmt;
    }

    public void setUserCouponAmt(BigDecimal userCouponAmt) {
        this.userCouponAmt = userCouponAmt;
    }

    public Integer getCouponExpireTime() {
        return couponExpireTime;
    }

    public void setCouponExpireTime(Integer couponExpireTime) {
        this.couponExpireTime = couponExpireTime;
    }

    public String getCouponExpireDate() {
        return couponExpireDate;
    }

    public void setCouponExpireDate(String couponExpireDate) {
        this.couponExpireDate = couponExpireDate;
    }
}