package com.zktravel.model.vo.product;

import com.zktravel.model.vo.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @CreateDate 2018-05-08
 */
@ApiModel("VO")
public class TaProductTopVO extends BaseVO {
    /**
     * 商品id
     */
    @ApiModelProperty("商品id")
    private Long itemId;
    /**
     * 商铺id
     */
    @ApiModelProperty("商铺id")
    private Long shopId;
    /**
     * 模块0大牌推荐1欧洲必买2新品上架
     */
    @ApiModelProperty("模块0大牌推荐1欧洲必买2新品上架")
    private Integer type;
    /**
     * 排序数量越小优先级越高
     */
    @ApiModelProperty("排序数量越小优先级越高")
    private Integer sort;
    /**
     * 品牌id
     */
    @ApiModelProperty("品牌id")
    private Long brandId;
    /**
     *
     */
    @ApiModelProperty("")
    private Integer status;

    @ApiModelProperty("排序类型")
    private Integer orderby;

    public void setProductId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getProductId() {
        return itemId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return type;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getSort() {
        return sort;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public Integer getOrderby() {
        return orderby;
    }

    public void setOrderby(Integer orderby) {
        this.orderby = orderby;
    }

}