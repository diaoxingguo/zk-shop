package com.zktravel.model.vo.product;

import com.zktravel.model.vo.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 *
 * @CreateDate 2018-05-08
 */
 @ApiModel("VO")
public class TaProductVO extends BaseVO {
	/**
	 * 商品id
	 */
	@ApiModelProperty("商品id")
	private Long productId;
	/**
	 * 0上架1下架
	 */
	@ApiModelProperty("0上架1下架")
	private Integer status;
	/**
	 * 售价
	 */
	@ApiModelProperty("售价")
	private BigDecimal sellPrice;

	/**
	 * 成本价
	 */
	@ApiModelProperty("成本价")
	private BigDecimal marketPrice;
	/**
	 * 利润
	 */
	@ApiModelProperty("利润")
	private BigDecimal profitAmt;
	/**
	 * 利润
	 */
	@ApiModelProperty("库存量")
	private Integer stockNum;
	/**
	 * 商品名
	 */
	@ApiModelProperty("商品名")
	private String name;

	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getStatus() {
		return status;
	}
	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}
	public BigDecimal getSellPrice() {
		return sellPrice;
	}
	public void setProfitAmt(BigDecimal profitAmt) {
		this.profitAmt = profitAmt;
	}
	public BigDecimal getProfitAmt() {
		return profitAmt;
	}
	public Integer getStockNum() {
		return stockNum;
	}
	public void setStockNum(Integer stockNum) {
		this.stockNum = stockNum;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
}