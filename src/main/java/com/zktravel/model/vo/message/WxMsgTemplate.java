package com.zktravel.model.vo.message;

import java.io.Serializable;
import java.util.List;

public class WxMsgTemplate implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<String> open_id;
	private Integer sleep = 0;
	private WxMsgPost post;
	
	public List<String> getOpen_id() {
		return open_id;
	}
	public void setOpen_id(List<String> open_id) {
		this.open_id = open_id;
	}
	public Integer getSleep() {
		return sleep;
	}
	public void setSleep(Integer sleep) {
		this.sleep = sleep;
	}
	public WxMsgPost getPost() {
		return post;
	}
	public void setPost(WxMsgPost post) {
		this.post = post;
	}
}
