package com.zktravel.model.vo.message;

import java.io.Serializable;

public class WxMsgPost implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String touser;
	private String template_id;
	private String url;
	private String topcolor;
	private Object data;
	private Object miniprogram;
	
	public String getTouser() {
		return touser;
	}
	public void setTouser(String touser) {
		this.touser = touser;
	}
	public String getTemplate_id() {
		return template_id;
	}
	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTopcolor() {
		return topcolor;
	}
	public void setTopcolor(String topcolor) {
		this.topcolor = topcolor;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}

	public Object getMiniprogram() {
		return miniprogram;
	}

	public void setMiniprogram(Object miniprogram) {
		this.miniprogram = miniprogram;
	}
}
