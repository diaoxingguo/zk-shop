#!/bin/bash

TOMPATH="/data/tomcat/apache-tomcat-8.5.31"

if [ -z $1 ]
then
    rm -fr target
    mvn compile
    mvn package
    echo "编译完成"
    rm -fr "$TOMPATH/webapps/zk-shop.war"
    cp "./target/zk-shop.war" "$TOMPATH/webapps/"
    "$TOMPATH/bin/shutdown.sh"
    "$TOMPATH/bin/startup.sh"
    echo "发布完成"
    exit
fi